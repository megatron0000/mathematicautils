(* ::Package:: *)

BeginPackage["ValueItems`"];


ValueItem::usage="
    ValueItem[date,name,value,metadata]. Example: ValueItem[Now[], 'income', 200, 'received payment']

    ValueItem[name, value, metadata]. date defaults to now.

    ValueItem[name,value]. date defaults to now and metadata defaults to Null
";


ValueItemRecords::usage="ValueItemRecords[] gets a list of all registered value items";


AddValueItem::usage="AddValueItem[item] appends new value item It already persists the result on storage"


PersistValueItems::usage="PersistValueItems[] saves the current state of ValueItemRecords[] to storage";


Begin["`Private`"];


database=FileNameJoin[{$UserDocumentsDirectory,"value-items-database"}];


If[
	FileExistsQ[database],
	Get[database],
	ValueItemRecords[]={}; Save[database, ValueItemRecords];
]


AddValueItem[item_ValueItem]:= Block[ {},
	ValueItemRecords[] = ValueItemRecords[] ~ Append ~ item;
	PersistValueItems[];
]


PersistValueItems[]:=Block[{},
	DeleteFile[database];
	Save[database, ValueItemRecords];
]

ValueItem[name_String, value_, metadata_] := ValueItem[DateObject[], name, value, metadata]

ValueItem[name_String, value_] := ValueItem[DateObject[], name, value, Null]


End[];


EndPackage[];
