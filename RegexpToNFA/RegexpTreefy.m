(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: RegexpTreefy *)
(* :Context: RegexpTreefy` *)
(* :Author: vitor *)
(* :Date: 2018-04-30 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["RegexpToNFA`RegexpTreefy`"];
(* Exported symbols added here with SymbolName::usage *)

BuildTree::usage =
    "BuildTree[regexp] builds a tree from the regexp string. Nodes are \"\[Bullet]\"[node1, node2],
\"\[Star]\"[node], \"\
\[VerticalSeparator]\"[node1, node2] and characters";

Begin["`Private`"];

<< GlobalThings`;

(* Formalization of a regexp grammar (Chomsky Normal Form) *)

(* Tokenize using normal characters (that is, nothing like \[Star\] ) *)

ClearAll[ToToken];
ToToken["*"] = Token["kleene star", "*"];
ToToken["+"] = Token["one or more", "+"];
ToToken["?"] = Token["maybe", "?"];
ToToken["|"] = Token["union", "|"];
ToToken["("] = Token["open paren", "("];
ToToken[")"] = Token["close paren", ")"];
ToToken[char_] := Token["character", char];

ClearAll[TokenRules];
TokenRules[
  "word class"] = {first___, Token["character", "\\"],
  Token["character", "w"], last___} :> {first,
  Token["character", "\\w"], last};
TokenRules[
  "newline class"] = {first___, Token["character", "\\"],
  Token["character", "n"], last___} :> {first,
  Token["character", "\n"], last};
(* We put a "ready" in the end so already processed items will not be \
changed again *)

TokenRules[
  "escaped \\"] = {first___, Token["character", "\\"],
  Token["character", "\\"], last___} :> {first,
  Token["character", "\\", ready], last};
TokenRules[
  "escaped |"] = {first___, Token["character", "\\"],
  Token["union", "|"], last___} :> {first,
  Token["character", "|", ready], last};
TokenRules[
  "escaped +"] = {first___, Token["character", "\\"],
  Token["one or more", "+"], last___} :> {first,
  Token["character", "+", ready], last};
TokenRules[
  "carriage-return class"] = {first___, Token["character", "\\"],
  Token["character", "r"], last___} :> {first,
  Token["character", "\r"], last};
TokenRules[
  "lowercase letters class"] = {first___, Token["character", "\\"],
  Token["character", "l"], last___} :> {first,
  Token["character", "\\l"], last};
TokenRules[
  "uppercase letters class"] = {first___, Token["character", "\\"],
  Token["character", "u"], last___} :> {first,
  Token["character", "\\u"], last};
TokenRules[
  "bracket class"] = {first___, Token["character", "["], middle__,
  Token["character", "]"], last___} :> {first,
  Token["character", "[]"[middle]], last};
TokenRules[
  "negated bracket class"] = {first___, Token["character", "^"],
  Token["character", "[]"[middle__]], last___} :> {first,
  Token["character", "^[]"[middle]], last};
TokenRules[
  "space-like class"] = {first___, Token["character", "\\"],
  Token["character", "s"], last___} :> {first,
  Token["character", "\\s"], last};
TokenRules[
  "space-like class"] = {first___, Token["character", "\\"],
  Token["character", "s"], last___} :> {first,
  Token["character", "\\s"], last};
TokenRules[
  "no remaining \\"] = {Token["character", "\\"] :>
    Throw["Not recognized \\ character sequence",
      UnrecognizedSequenceError]};
TokenRules[
  "renormalize ready escaped sequences"] = {first___,
  Token[content__, ready], last___} :> {first, Token[content],
  last};

ClearAll[Tokenize];
Tokenize[string_] :=
    Map[ToToken, StringSplit[string, ""]] //.
        TokenRules["escaped \\"] //.
        TokenRules["escaped |"] //. TokenRules["escaped +"] //.
        TokenRules["word class"] //.
        TokenRules["newline class"] //.
        TokenRules["carriage-return class"] //.
        TokenRules["lowercase letters class"] //.
        TokenRules["uppercase letters class"] //.
        TokenRules["bracket class"] //.
        TokenRules["negated bracket class"] //.
        TokenRules["space-like class"] //.
        TokenRules["no remaining \\"] //.
        TokenRules["renormalize ready escaped sequences"];

(* Productions *)

Productions[
  Reg] = {{Subreg, Subreg}, {A, Reg}, {B, C}, {D, E}, {Paren, E},
  Token["character", _]};
Productions[Subreg] = {{Subreg, Subreg}, {B, C}, {D, E}, {Paren, E},
  Token["character", _], {Paren, Subreg}};
Productions[D] = {Token["character", _]};
Productions[E] = {Token["kleene star", "*"],
  Token["one or more", "+"], Token["maybe", "?"]};
Productions[F] = {Token["union", "|"]};
Productions[G] = {Token["open paren", "("]};
Productions[A] = {{Reg, F}};
Productions[B] = {{G, Reg}};
Productions[C] = {Token["close paren", ")"]};
Productions[Paren] = {{B, C}};

ClearAll[NonTerminals];
NonTerminals[] = {Reg, Subreg, Paren, A, B, C, D, E, F, G};

ClearAll[BuildAntiProductions];
BuildAntiProductions[] := Block[{},
  Map[
    Function[
      {nonTerminal},
      Map[
        Function[
          {production},
          AntiProductions[production] =
              If[! ValueQ[AntiProductions[production]], {nonTerminal},
                AntiProductions[production] ~ Append ~ nonTerminal]
        ],
        Productions[nonTerminal]
      ]
    ],
    NonTerminals[]
  ];
  AntiProductions[else_] := {}
];

BuildAntiProductions[];

(* Cocke-Younger-Kasami *)

ClearAll[CYK];
CYK[tokens_List] := Block[{n, i, j, table},
  table = MapIndexed[
    Block[
      {row},
      row = Table[Null, Length[tokens]];
      row[[#2[[1]]]] =
          Select[NonTerminals[], Function[{nonTerminal},
            If[#1 ~ ElementPattern ~ Productions[nonTerminal],
              MotivationFor[nonTerminal, {#2[[1]], #2[[1]]}] =
                  Motivation[{#1, Null}]; True, False]
          ]];
      row
    ]&,
    tokens
  ];
  For[n = 2, n <= Length[table], n++,
    For[i = 1, i <= Length[table] - n + 1, i++,
      j = i + n - 1;
      table[[i, j]] =
          DeleteDuplicates@
              Flatten[AntiProductions /@ DeleteDuplicates@Flatten[#, 1] &@
                  Table[
                    Cartesian[table[[i, k]], table[[k + 1, j]]],
                    {k, i, j - 1}
                  ]
              ];
      MapIndexed[
        If[AntiProductions[#1] != {},
          Map[Function[{nonTerminal},
            MotivationFor[nonTerminal, {i, j}] =
                Motivation[{#1[[1]], {i, i + #2[[1]] - 1}}, {#1[[
                    2]], {i + #2[[1]], j}}]],
            DeleteDuplicates[AntiProductions[#1]]]] &, #, {2}] &@
          Map[table[[#[[1, 1]], #[[1, 2]]]] ~ Cartesian ~
              table[[#[[2, 1]], #[[2, 2]]]] &, #] &@Table[
        {Posit[i, k], Posit[k + 1, j]},
        {k, i, j - 1}
      ];
    ];
  ];
  table
];

(* Rules for tree modification. Introduces special characters, like \[Star] *)

(* Some rules have to be processed before others *)

ClearAll[FirstRules];
FirstRules["no virtual Reg|Subreg"] = {Reg[onlyChild_] :> onlyChild,
  Subreg[onlyChild_] :> onlyChild};
FirstRules["no meaningless nonterminals"] = {G[token_] :> token,
  B[children__] :> children, C[token_] :> token, F[token_] :> token,
  E[token_] :> token, A[children__] :> children};
FirstRules[
  "reduce parentheses"] = {Reg["(", subtree_, ")"] :> subtree,
  Subreg["(", subtree_, ")"] :> subtree,
  Paren["(", subtree_, ")"] :> subtree};
FirstRules[
  "reduce concat"] = {Reg[subtree1_, "|", subtree2_] :>
    "\[VerticalSeparator]"[subtree1, subtree2]};
FirstRules[
  "reduce kleene-like"] = {Subreg[subtree_, "*"] :>
    "\[Star]"[subtree], Reg[subtree_, "*"] :> "\[Star]"[subtree],
  Subreg[subtree_, "+"] :> "+"[subtree],
  Reg[subtree_, "+"] :> "+"[subtree],
  Subreg[subtree_, "?"] :> "?"[subtree],
  Reg[subtree_, "?"] :> "?"[subtree]};
FirstRules["only token lexemes"] = {Token[name_, lexeme_] :> lexeme};


ClearAll[SecondRules];
SecondRules[
  "no virtual Reg|Subreg"] = {Reg[child1_, child2_] :>
    "\[Bullet]"[child1, child2],
  Subreg[child1_, child2_] :> "\[Bullet]"[child1, child2]};

(* Reduce extra operators to classic set *)

ClearAll[ExtraRules];
ExtraRules[
  "no + operator"] = {"+"[subtree_] :>
    "\[Bullet]"[subtree, "\[Star]"[subtree]]};
ExtraRules[
  "no ? operator"] = {"?"[subtree_] :>
    "\[VerticalSeparator]"[subtree, "\[Epsilon]"]};
(* ExtraRules["no \\w character class"]={"\\w"\[Rule] Fold["\
\[VerticalSeparator]",Flatten@{Through[{Identity,ToUpperCase}[\
Alphabet[]]],ToString/@{0,1,2,3,4,5,6,7,8,9}} ]}; *)


(* Joining rules easily *)

ClearAll[JoinRules];
JoinRules[which_] := Flatten@(Join @@ Values[DownValues[which]]);


(* Tree building *)

ClearAll[BuildTreeInternal];
BuildTreeInternal[root_, position_] :=
    root @@ Map[BuildTreeInternal @@ # &, MotivationFor[root, position]];

BuildTreeInternal[root_, Null] := root;

BuildTree[regexp_String] := Block[
  {result, table, tokens},
  tokens = Tokenize[regexp];
  table = CYK[tokens];
  If[TrueQ[! Reg \[Element] table[[1, Length[tokens]]]],
    Throw["Invalid Regexp", InvalidRegexpError]];
  result =
      BuildTreeInternal[Reg, {1, Length[tokens]}] //.
          JoinRules[FirstRules] //. JoinRules[SecondRules] //.
          JoinRules[ExtraRules];
  ClearAll[MotivationFor];
  result
];

End[]; (* `Private` *)

EndPackage[];
