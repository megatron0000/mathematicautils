    (* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: RegexpToNFA *)
(* :Context: RegexpToNFA` *)
(* :Author: vitor *)
(* :Date: 2018-05-16 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

(* Public *)

BeginPackage["RegexpToNFA`"];

NodeToNFA::usage =
    "NodeToNFA[node] may be called on \"\[Bullet]\", \"\
\[VerticalSeparator]\", \"\[Star]\" nodes and on characters";

RegexpToNFA::usage =
    "RegexpToNFA[string] converts regexp string into an NFA";

(* Build NFA from Regexp tree *)



Begin["`Private`"];

<< RegexpToNFA`RegexpTreefy`;
<< GlobalThings`;
<< Automaton`NFABuilder`;


NodeToNFA[node_] := Block[
  {startState = NFAState[], acceptState = NFAState[], nfa = NFA[]},
  AddTransition[Transition[startState, acceptState, node]];
  MakeStart[nfa, startState];
  MakeAccepting[nfa, acceptState];
  nfa
];
NodeToNFA["\\u"] := Block[
  {start = NFAState[], accept = NFAState[], nfa = NFA[]},
  Map[AddTransition[Transition[start, accept, #]] &,
    ToUpperCase /@ Alphabet[]];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["\\l"] := Block[
  {start = NFAState[], accept = NFAState[], nfa = NFA[]},
  Map[AddTransition[Transition[start, accept, #]] &, Alphabet[]];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["\\s"] := Block[
  {start = NFAState[], accept = NFAState[], nfa = NFA[]},
  Map[AddTransition[Transition[start, accept, #]] &, {" ", "\t"}];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["."] := Block[
  {start = NFAState[], accept = NFAState[], nfa = NFA[]},
  AddTransition[Transition[start, accept, _]];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["\\w"] := Block[
  {start = NFAState[], accept = NFAState[], nfa = NFA[]},
  Map[AddTransition[Transition[start, accept, #]] &, #] &@
      Flatten[{Through[{Identity, ToUpperCase}[Alphabet[]]],
        ToString /@ {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, "_"}];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["\[VerticalSeparator]"[left_, right_]] := Block[
  {start = NFAState[], accept = NFAState[],
    leftNFA = NodeToNFA[left], rightNFA = NodeToNFA[right],
    nfa = NFA[]},
  AddTransition[Transition[start, #, "\[Epsilon]"]] & /@ {StartState[
    leftNFA], StartState[rightNFA]};
  AddTransition[
    Transition[#, accept, "\[Epsilon]"]] & /@ (AcceptStates[
    leftNFA] \[Union] AcceptStates[rightNFA]);
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["\[Bullet]"[left_, right_]] := Block[
  {nfa = NFA[], leftNFA = NodeToNFA[left],
    rightNFA = NodeToNFA[right]},
  AddTransition[
    Transition[#, StartState[rightNFA], "\[Epsilon]"]] & /@
      AcceptStates[leftNFA];
  MakeStart[nfa, StartState[leftNFA]];
  MakeAccepting[nfa, #] & /@ AcceptStates[rightNFA];
  nfa
];
NodeToNFA["\[Star]"[child_]] := Block[
  {nfa = NFA[], start = NFAState[], accept = NFAState[],
    childNFA = NodeToNFA[child]},
  AddTransition[Transition[start, accept, "\[Epsilon]"]];
  AddTransition[
    Transition[start, StartState[childNFA], "\[Epsilon]"]];
  (AddTransition[Transition[#, StartState[childNFA], "\[Epsilon]"]];
  AddTransition[Transition[#, accept, "\[Epsilon]"]]) & /@
      AcceptStates[childNFA];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["[]"[children__]] := Block[
  {nfa = NFA[], start = NFAState[], accept = NFAState[],
    childrenNFA =
        NodeToNFA /@
            Function[child, _?(# ~ MatchQ ~ child &)] /@ {children}},
  AddTransition[Transition[start, #, "\[Epsilon]"]] & /@
      StartState /@ childrenNFA;
  AddTransition[Transition[#, accept, "\[Epsilon]"]] & /@
      Flatten[AcceptStates /@ childrenNFA];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];
NodeToNFA["^[]"[children__]] := Block[
  {nfa = NFA[], start = NFAState[], accept = NFAState[],
    childrenNFA =
        NodeToNFA /@
            Function[child, _?(Not[# ~ MatchQ ~ child] &)] /@ {children}},
  AddTransition[Transition[start, StartState[#], "\[Epsilon]"]] & /@
      childrenNFA;
  AddTransition[Transition[#, accept, "\[Epsilon]"]] & /@
      Flatten[AcceptStates /@ childrenNFA];
  MakeStart[nfa, start];
  MakeAccepting[nfa, accept];
  nfa
];


RegexpToNFA[string_] := NodeToNFA@BuildTree@string;



End[];
EndPackage[];
