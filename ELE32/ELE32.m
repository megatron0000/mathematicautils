(* ::Package:: *)

BeginPackage["ELE32`"];

RaisedCosine::usage = 
"RaisedCosine[T, beta] gives a raised cosine with roll-off beta and
symbol duration T. RaisedCosine[T, beta][t] evaluates it at time t";

Energy::usage =
"Energy[signal, t] gives signal energy in variable t";

Seq::usage="Seq[rules, k] constructs a piecewise function. Example: Seq[{0 -> a, 1 -> b}, k]";

Begin["Private`"];

RaisedCosine[T_, beta_][t_] := (1/T) Sinc[Pi t/T] (Cos[Pi beta t/T] / (1-(2 beta t/T)^2))

Energy[signal_, t_] := Integrate[signal^2, {t, -Infinity, +Infinity}]

Unprotect[FourierTransform];
FourierTransform[RaisedCosine[T_, beta_], t_, f_] := 
	Piecewise[{
		{T, Abs[f] < (1-beta)/(2T)},
		{T/2 (1+Cos[Pi T/beta (Abs[f] - (1-beta)/(2T))] ), 
			(1-beta)/(2T) <= Abs[f] < (1+beta)/(2T) },
		{0, True}
	}]
Protect[FourierTransform];

Seq[rules_,k_]:=Piecewise[({Last@#,k==First@#}&)/@rules]

End[];

EndPackage[];
