(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     36612,        988]
NotebookOptionsPosition[     31675,        880]
NotebookOutlinePosition[     35548,        963]
CellTagsIndexPosition[     35505,        960]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"LoadPackage", "@", "\"\<LexerGenerator`\>\""}]], "Input",ExpressionU\
UID->"11ee32f8-86e4-4446-8615-24d1c753c33a"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TimeConstrained", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"t", "=", 
     RowBox[{"BuildTokenizer", "[", 
      RowBox[{"ReadString", "[", "\"\<test.txt\>\"", "]"}], "]"}]}], ";", 
    "\n", 
    RowBox[{
     RowBox[{"t", "[", "\"\<do re /2 mi mais rapido sol/2\>\"", "]"}], "//", 
     "MatrixForm"}]}], ",", "\[IndentingNewLine]", "3"}], 
  "\[IndentingNewLine]", "]"}]], "Input",ExpressionUUID->"0404eceb-60d3-4e77-\
9773-e25e1a3a384a"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", 
   TagBox[GridBox[{
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"do\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"re\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"duracao\"\>", ",", "\<\"/2\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"mi\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"comando\"\>", ",", "\<\"mais rapido\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"sol\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"duracao\"\>", ",", "\<\"/2\"\>"}], "]"}]}
     },
     GridBoxAlignment->{
      "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
        "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Column], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",ExpressionUUID->"a4166b57-88de-4b32-\
849b-5c5617ea4136"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"LoadPackage", "@", "\"\<AutomatonSimulator`\>\""}]], "Input",Express\
ionUUID->"2a749f69-47d4-4f17-86dc-cd323e8b65e0"],

Cell[BoxData[
 RowBox[{"LoadPackage", "@", "\"\<RegexpToNFA`\>\""}]], "Input",ExpressionUUID->"7b1d4a83-cf15-40ee-bc39-fc197db2691d"],

Cell[BoxData[
 RowBox[{"LoadPackage", "@", "\"\<NFAToDFA`\>\""}]], "Input",ExpressionUUID->"52abc63c-83ed-4c6b-887e-9ec0ac833338"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DFAMatch", "[", 
  RowBox[{
   RowBox[{"NFAToDFA", "@", 
    RowBox[{"RegexpToNFA", "[", "\"\<( +solene)?\>\"", "]"}]}], ",", 
   "\"\< solene\>\""}], "]"}]], "Input",ExpressionUUID->"803b0ff6-7470-4379-\
85ca-5948f1d70d0a"],

Cell[BoxData["True"], "Output",ExpressionUUID->"5146fa71-7ea6-4bf7-8d54-a5dcb411c40c"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"LoadPackage", "@", "\"\<RegexpTreefy`\>\""}]], "Input",ExpressionUUID->"38986a15-6bac-4155-b88f-9996cfcc8cc5"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TreeForm", "@", 
  RowBox[{
  "BuildTree", "[", "\"\<c +(a|b)( +d)?|(e|f) +g\>\"", "]"}]}]], "Input",Expre\
ssionUUID->"eff225ad-72b0-44f1-9bff-1ef06f5da5d7"],

Cell[BoxData[
 GraphicsBox[
  TagBox[GraphicsComplexBox[CompressedData["
1:eJxTTMoPSmViYGCQA2IQPXW1JY9MlIBDn/3P33qFgg4GUYutPZ4yOUwX8fr5
Q5AXKv7RXmjVwl+THdkdhMH0dXuIOKMDAxRA5L/bQ+Sf26Pyr9sLQMVh5qHz
YebMANv7F26+MNQcmHkwcRh/BtSdMPeF3v9aeUyTywFVPztc/cUS3+rESBYH
mP0w9eh8VPfwwvXf8pxXof2LH818cXh4FfV/31V1VhTuHkh4CsHVw8IZ1T0i
cP6WI95Wa7dKYJgPk4e4TwqDD3M/TD/MfgABjd1Y
    "], {{
      {RGBColor[0.55, 0.45, 0.45], 
       LineBox[{{1, 2}, {1, 21}, {2, 3}, {2, 13}, {3, 4}, {3, 10}, {4, 5}, {4,
         6}, {6, 7}, {6, 8}, {8, 9}, {10, 11}, {10, 12}, {13, 14}, {13, 20}, {
        14, 15}, {14, 19}, {15, 16}, {15, 17}, {17, 18}, {21, 22}, {21, 30}, {
        22, 23}, {22, 26}, {23, 24}, {23, 25}, {26, 27}, {26, 28}, {28, 
        29}}]}}, {
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[VerticalSeparator]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 1],
        TagBox[
         RowBox[{"\"\[VerticalSeparator]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\"c\"", ",", 
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], 
                    "]"}], ",", 
                    RowBox[{"\"\[VerticalSeparator]\"", "[", 
                    RowBox[{"\"a\"", ",", "\"b\""}], "]"}]}], "]"}], ",", 
                 RowBox[{"\"\[VerticalSeparator]\"", "[", 
                   RowBox[{
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], ",", 
                    "\"d\""}], "]"}], ",", "\"\[Epsilon]\""}], "]"}]}], "]"}],
              ",", 
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{
                    RowBox[{"\"\[VerticalSeparator]\"", "[", 
                    RowBox[{"\"e\"", ",", "\"f\""}], "]"}], ",", 
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], 
                   "]"}], ",", "\"g\""}], "]"}]}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[VerticalSeparator]"[
          "\[Bullet]"[
           "\[Bullet]"[
            "\[Bullet]"["c", 
             "\[Bullet]"[" ", 
              "\[Star]"[" "]]], 
            "\[VerticalSeparator]"["a", "b"]], 
           "\[VerticalSeparator]"[
            "\[Bullet]"[
             "\[Bullet]"[" ", 
              "\[Star]"[" "]], "d"], "\[Epsilon]"]], 
          "\[Bullet]"[
           "\[Bullet]"[
            "\[VerticalSeparator]"["e", "f"], 
            "\[Bullet]"[" ", 
             "\[Star]"[" "]]], "g"]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 2],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{"\"c\"", ",", 
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], 
                   "]"}], ",", 
                 RowBox[{"\"\[VerticalSeparator]\"", "[", 
                   RowBox[{"\"a\"", ",", "\"b\""}], "]"}]}], "]"}], ",", 
             RowBox[{"\"\[VerticalSeparator]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{
                    RowBox[{"\"\[Bullet]\"", "[", 
                    RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], ",", 
                    "\"d\""}], "]"}], ",", "\"\[Epsilon]\""}], "]"}]}], "]"}],
          HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[
          "\[Bullet]"[
           "\[Bullet]"["c", 
            "\[Bullet]"[" ", 
             "\[Star]"[" "]]], 
           "\[VerticalSeparator]"["a", "b"]], 
          "\[VerticalSeparator]"[
           "\[Bullet]"[
            "\[Bullet]"[" ", 
             "\[Star]"[" "]], "d"], "\[Epsilon]"]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 3],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{"\"c\"", ",", 
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], 
               "]"}], ",", 
             RowBox[{"\"\[VerticalSeparator]\"", "[", 
               RowBox[{"\"a\"", ",", "\"b\""}], "]"}]}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[
          "\[Bullet]"["c", 
           "\[Bullet]"[" ", 
            "\[Star]"[" "]]], 
          "\[VerticalSeparator]"["a", "b"]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 4],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{"\"c\"", ",", 
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{"\" \"", ",", 
                 RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], "]"}],
          HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"["c", 
          "\[Bullet]"[" ", 
           "\[Star]"[" "]]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"c\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 5],
        TagBox["\"c\"", HoldForm]],
       Annotation[#, 
        HoldForm["c"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 6],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{"\" \"", ",", 
             RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[" ", 
          "\[Star]"[" "]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 7],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Star]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 8],
        TagBox[
         RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Star]"[" "]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 9],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[VerticalSeparator]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 10],
        TagBox[
         RowBox[{"\"\[VerticalSeparator]\"", "[", 
           RowBox[{"\"a\"", ",", "\"b\""}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[VerticalSeparator]"["a", "b"]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"a\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 11],
        TagBox["\"a\"", HoldForm]],
       Annotation[#, 
        HoldForm["a"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"b\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 12],
        TagBox["\"b\"", HoldForm]],
       Annotation[#, 
        HoldForm["b"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[VerticalSeparator]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 13],
        TagBox[
         RowBox[{"\"\[VerticalSeparator]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], ",", 
                 "\"d\""}], "]"}], ",", "\"\[Epsilon]\""}], "]"}], HoldForm]],
       
       Annotation[#, 
        HoldForm[
         "\[VerticalSeparator]"[
          "\[Bullet]"[
           "\[Bullet]"[" ", 
            "\[Star]"[" "]], "d"], "\[Epsilon]"]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 14],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{"\" \"", ",", 
                 RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], ",", 
             "\"d\""}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[
          "\[Bullet]"[" ", 
           "\[Star]"[" "]], "d"]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 15],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{"\" \"", ",", 
             RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[" ", 
          "\[Star]"[" "]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 16],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Star]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 17],
        TagBox[
         RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Star]"[" "]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 18],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"d\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 19],
        TagBox["\"d\"", HoldForm]],
       Annotation[#, 
        HoldForm["d"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"\[Epsilon]\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 20],
        TagBox["\"\[Epsilon]\"", HoldForm]],
       Annotation[#, 
        HoldForm["\[Epsilon]"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 21],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{
                 RowBox[{"\"\[VerticalSeparator]\"", "[", 
                   RowBox[{"\"e\"", ",", "\"f\""}], "]"}], ",", 
                 RowBox[{"\"\[Bullet]\"", "[", 
                   RowBox[{"\" \"", ",", 
                    RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], 
               "]"}], ",", "\"g\""}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[
          "\[Bullet]"[
           "\[VerticalSeparator]"["e", "f"], 
           "\[Bullet]"[" ", 
            "\[Star]"[" "]]], "g"]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 22],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{
             RowBox[{"\"\[VerticalSeparator]\"", "[", 
               RowBox[{"\"e\"", ",", "\"f\""}], "]"}], ",", 
             RowBox[{"\"\[Bullet]\"", "[", 
               RowBox[{"\" \"", ",", 
                 RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}]}], "]"}],
          HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[
          "\[VerticalSeparator]"["e", "f"], 
          "\[Bullet]"[" ", 
           "\[Star]"[" "]]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[VerticalSeparator]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 23],
        TagBox[
         RowBox[{"\"\[VerticalSeparator]\"", "[", 
           RowBox[{"\"e\"", ",", "\"f\""}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[VerticalSeparator]"["e", "f"]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"e\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 24],
        TagBox["\"e\"", HoldForm]],
       Annotation[#, 
        HoldForm["e"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"f\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 25],
        TagBox["\"f\"", HoldForm]],
       Annotation[#, 
        HoldForm["f"], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Bullet]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 26],
        TagBox[
         RowBox[{"\"\[Bullet]\"", "[", 
           RowBox[{"\" \"", ",", 
             RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}]}], "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Bullet]"[" ", 
          "\[Star]"[" "]]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 27],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox["\<\"\[Star]\"\>", "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 28],
        TagBox[
         RowBox[{"\"\[Star]\"", "[", "\" \"", "]"}], HoldForm]],
       Annotation[#, 
        HoldForm[
         "\[Star]"[" "]], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\" \"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 29],
        TagBox["\" \"", HoldForm]],
       Annotation[#, 
        HoldForm[" "], "Tooltip"]& ], 
      TagBox[
       TooltipBox[InsetBox[
         FrameBox[
          StyleBox[
           TagBox["\<\"g\"\>",
            HoldForm], "StandardForm", "Output",
           StripOnInput->False,
           LineColor->GrayLevel[0],
           FrontFaceColor->GrayLevel[0],
           BackFaceColor->GrayLevel[0],
           GraphicsColor->GrayLevel[0],
           FontSize->Scaled[0.05],
           FontColor->GrayLevel[0]],
          Background->RGBColor[1., 1., 0.871],
          FrameStyle->GrayLevel[0.5],
          StripOnInput->False], 30],
        TagBox["\"g\"", HoldForm]],
       Annotation[#, 
        HoldForm["g"], "Tooltip"]& ]}}],
   Annotation[#, VertexCoordinateRules -> CompressedData["
1:eJxTTMoPSmViYGCQA2IQPXW1JY9MlIBDn/3P33qFgg4GUYutPZ4yOUwX8fr5
Q5AXKv7RXmjVwl+THdkdhMH0dXuIOKMDAxRA5L/bQ+Sf26Pyr9sLQMVh5qHz
YebMANv7F26+MNQcmHkwcRh/BtSdMPeF3v9aeUyTywFVPztc/cUS3+rESBYH
mP0w9eh8VPfwwvXf8pxXof2LH818cXh4FfV/31V1VhTuHkh4CsHVw8IZ1T0i
cP6WI95Wa7dKYJgPk4e4TwqDD3M/TD/MfgABjd1Y
     "]]& ],
  AspectRatio->NCache[Rational[7, 5]^Rational[1, 2], 1.1832159566199232`],
  FormatType:>StandardForm,
  FrameTicks->Automatic,
  ImageSize->{339.1557320751381, Automatic},
  PlotRange->All,
  PlotRangePadding->Scaled[0.1]]], "Output",ExpressionUUID->"47057329-3d66-\
4b79-af0f-c7e2a68833aa"]
}, Open  ]]
},
WindowSize->{1855, 1028},
WindowMargins->{{0, Automatic}, {0, Automatic}},
InputAliases->{"intt" -> RowBox[{"\[Integral]", 
     RowBox[{"\[SelectionPlaceholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "dintt" -> 
  RowBox[{
     SubsuperscriptBox[
     "\[Integral]", "\[SelectionPlaceholder]", "\[Placeholder]"], 
     RowBox[{"\[Placeholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "sumt" -> RowBox[{
     UnderoverscriptBox["\[Sum]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "prodt" -> RowBox[{
     UnderoverscriptBox["\[Product]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "dt" -> RowBox[{
     SubscriptBox["\[PartialD]", "\[Placeholder]"], " ", 
     "\[SelectionPlaceholder]"}], "ia" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Inactive"], "cbrt" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "3", SurdForm -> True, MultilineFunction -> 
    None], "surd" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "\[Placeholder]", SurdForm -> True, 
    MultilineFunction -> None], "ket" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Ket"], "bra" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Bra"], "braket" -> 
  TemplateBox[{"\[SelectionPlaceholder]", "\[Placeholder]"}, "BraKet"], 
  "delay" -> TemplateBox[{"\[SelectionPlaceholder]"}, "SystemsModelDelay"], 
  "grad" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Placeholder]"}], 
  "del." -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], ".", 
     "\[Placeholder]"}], "delx" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Cross]", 
     "\[Placeholder]"}], "del2" -> RowBox[{
     SubsuperscriptBox["\[Del]", "\[SelectionPlaceholder]", 2], 
     "\[Placeholder]"}], "kd" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "KroneckerDeltaSeq"], "notation" -> 
  RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation>" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation<" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "symb" -> RowBox[{"Symbolize", "[", 
     TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], "]"}], 
  "infixnotation" -> RowBox[{"InfixNotation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], ",", 
       "\[Placeholder]"}], "]"}], "addia" -> RowBox[{"AddInputAlias", "[", 
     RowBox[{"\"\[SelectionPlaceholder]\"", "\[Rule]", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "pattwraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationPatternTag"], 
  "madeboxeswraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationMadeBoxesTag"], "upa" -> 
  RowBox[{"\[Placeholder]", "\[LeftArrow]", "\[Placeholder]"}], "uupa" -> 
  RowBox[{"\[Placeholder]", 
     OverscriptBox["\[LeftArrow]", "\[Placeholder]"], "\[Placeholder]"}]},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (March 13, 2017)",
StyleDefinitions->FrontEnd`FileName[{"Report"}, "StandardReport.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 138, 2, 41, "Input", "ExpressionUUID" -> \
"11ee32f8-86e4-4446-8615-24d1c753c33a"],
Cell[CellGroupData[{
Cell[721, 26, 488, 12, 132, "Input", "ExpressionUUID" -> \
"0404eceb-60d3-4e77-9773-e25e1a3a384a"],
Cell[1212, 40, 1965, 54, 263, "Output", "ExpressionUUID" -> \
"a4166b57-88de-4b32-849b-5c5617ea4136"]
}, Open  ]],
Cell[3192, 97, 142, 2, 41, "Input", "ExpressionUUID" -> \
"2a749f69-47d4-4f17-86dc-cd323e8b65e0"],
Cell[3337, 101, 133, 1, 41, "Input", "ExpressionUUID" -> \
"7b1d4a83-cf15-40ee-bc39-fc197db2691d"],
Cell[3473, 104, 130, 1, 41, "Input", "ExpressionUUID" -> \
"52abc63c-83ed-4c6b-887e-9ec0ac833338"],
Cell[CellGroupData[{
Cell[3628, 109, 248, 6, 43, "Input", "ExpressionUUID" -> \
"803b0ff6-7470-4379-85ca-5948f1d70d0a"],
Cell[3879, 117, 86, 0, 41, "Output", "ExpressionUUID" -> \
"5146fa71-7ea6-4bf7-8d54-a5dcb411c40c"]
}, Open  ]],
Cell[3980, 120, 134, 1, 41, "Input", "ExpressionUUID" -> \
"38986a15-6bac-4155-b88f-9996cfcc8cc5"],
Cell[CellGroupData[{
Cell[4139, 125, 182, 4, 41, "Input", "ExpressionUUID" -> \
"eff225ad-72b0-44f1-9bff-1ef06f5da5d7"],
Cell[4324, 131, 27335, 746, 425, "Output", "ExpressionUUID" -> \
"47057329-3d66-4b79-af0f-c7e2a68833aa"]
}, Open  ]]
}
]
*)

