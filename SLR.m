(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: SLR *)
(* :Context: SLR` *)
(* :Author: vitor *)
(* :Date: 2018-06-10 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["SLR`", {"GlobalThings`", "CFGrammar`", "Automaton`NFABuilder`"}];
(* Exported symbols added here with SymbolName::usage *)

GrammarItem::usage =
    "GrammarItem[production, position] represents a CFG item, in which \
position is an integer";

Closure::usage =
    "Closure[grammar, item] gives the LR(0) closure of the lone item

  Closure[grammar, itemList] gives the closure of the list";

NextSymbol::usage =
    "NextSymbol[item] gives the LR(0) item's next symbol or Null if \
item is already at the last position";

AdvanceItem::usage =
    "AdvanceItem[item] produces another item or throws if original item \
cannot be advanced further";

LR0DFA::usage =
    "LR0DFA[grammar] returns an NFA (which, in practice, behaves like a \
DFA), representing visually the behavior of an LR(0) parser for input \
grammar";

SLRParserFor::usage =
    "SLRParserFor[grammar] returns a function that parses Lists of \
Tokens according to 'grammar'";

Begin["`Private`"];

ClearAll[ItemPosition, ItemProduction, MaybeProductions, ClosureHelper];

ItemPosition[GrammarItem[production_, position_]] := position;

ItemProduction[GrammarItem[production_, position_]] := production;

NextSymbol[item_GrammarItem] :=
    If[
      ItemPosition[item] > Length@ProductionBody@ItemProduction[item],
      Null,
      ProductionBody[ItemProduction[item]][[ItemPosition[item]]]
    ];

MaybeProductions[grammar_Grammar, something_] :=
    If[
      something ~ MatchQ ~ (Token[__] | Null),
      {},
      Productions[grammar, something]
    ];

ClosureHelper[grammar_Grammar, item_GrammarItem, processedList_] :=
    Block[{},
      processedList[item] = True;
      Sow[item];
      Scan[ClosureHelper[grammar, #, processedList] &,
        Select[
          Map[
            GrammarItem[#, 1] &,
            MaybeProductions[grammar, NextSymbol[item] ]
          ],
          Not[processedList[#] ~ MatchQ ~ True] & ]
      ];
    ];

Closure[grammar_Grammar, item_GrammarItem] := Flatten@Last@Reap[
  ClosureHelper[grammar, item, Unique[]];
];

Closure[grammar_Grammar, itemList_List] := Union @@ Map[Closure[grammar, #] &, itemList];

AdvanceItem[item_GrammarItem] := If[
  NextSymbol[item] ~ MatchQ ~ Null,
  Throw[StringForm["Tried to advance a last-pos grammar item `1` ", item], UnadvanceableItemError],
  GrammarItem[ItemProduction[item], ItemPosition[item] + 1]
];

(* I want a better name *)

LR0DFA[grammar_Grammar] := LR0DFA[grammar] =
    Module[
      {
        I0, nfa = NFA[], currentItemSet, discoveredQ, items, state,
        oldStart = StartSymbol[grammar], mapper, parserStates
      },

      AddProduction[
        grammar,
        Production[NonTerminal["\[CapitalSigma]'"], {StartSymbol[grammar]}]
      ];

      MakeStartSymbol[grammar, NonTerminal["\[CapitalSigma]'"]];

      I0 = Closure[grammar, Map[GrammarItem[#, 1] & , Productions[grammar, NonTerminal["\[CapitalSigma]'"]] ]];

      discoveredQ[_] := False;
      discoveredQ[I0] = True;

      state[any_] := state[any] = NFAState[];

      MakeStart[nfa, state[I0]];

      MakeAccepting[nfa, state[
        Closure[grammar, AdvanceItem /@ Select[I0, NextSymbol[#] ~ MatchQ ~ oldStart & ]]
      ]];

      items = {I0};

      While[
        Length[items] != 0,
        currentItemSet = First[items];
        mapper[currentItemSet] = state[currentItemSet];
        mapper[state[currentItemSet]] = currentItemSet;
        parserStates ~ UpAppend ~ currentItemSet;
        items = Delete[items, 1];
        Scan[
          Function[{childRule},
            AddTransition[
              Transition[state[currentItemSet], state[Last@childRule], First@childRule]
            ];
            If[! discoveredQ[Last@childRule],
              AppendTo[items, Last@childRule];
              discoveredQ[Last@childRule] = True;
            ]
          ],
          Map[
            Closure[grammar, #] &,
            Map[
              AdvanceItem,
              Delete[GroupBy[currentItemSet, NextSymbol], Key[Null]],
              {2}
            ]
          ] /. Association -> List
        ];
      ];
      {nfa, mapper, parserStates}
    ];

SLRParserFor[grammarIn_Grammar] := Module[
  {
    grammar = CloneGrammar[grammarIn], oldStart, nfa, mapper,
    parserStates, ACTION, GOTO, REDUCE, SHIFT, ACCEPT,
    parserInitialState
  },
  ACTION[pState_, token_] := Null;
  GOTO[pState_, nonTerminal_] := Null;
  oldStart = StartSymbol[grammar];
  {nfa, mapper, parserStates} = LR0DFA[grammar];
  parserInitialState = mapper@StartState[nfa];
  ComputeFunctions[grammar];

  Scan[
    Function[{pState, nfaState},

    (* Shift actions (for terminals) and Goto (for nonTerminals) *)
      Scan[
        If[
          # ~ MatchQ ~ Token[__],
          ACTION[pState, #] = SHIFT[mapper @@ TransitionEndpoints[nfaState, #]],
          GOTO[pState, #] = mapper @@ TransitionEndpoints[nfaState, #];
        ] &,
        TransitionChars[nfaState]
      ];

      (* Reduce actions *)
      Scan[
        Function[production,
          Scan[
            If[
              Not[ACTION[pState, #] ~ MatchQ ~ Null],
              Throw[
                StringForm["Shift/reduce conflict on state `1` due to token `2`, which was already associated to `3` and was about to be associated to `4`",
                  Style[pState, Red],
                  Style[#, Green],
                  Style[ACTION[pState, #], Blue],
                  Style[REDUCE[production], Blue]
                ],
                ShiftReduceConflictError
              ],
              ACTION[pState, #] = REDUCE[production]
            ] &,
            FOLLOW[grammar, ProductionNonTerminal@production]
          ];
        ],
        ItemProduction /@ Select[
          pState,
          NextSymbol[#] ~ MatchQ ~ Null && Not[
            ProductionNonTerminal@ItemProduction[#] ~ MatchQ ~ NonTerminal["\[CapitalSigma]'"]
          ] &
        ]
      ];

      (* Accept action. Surely there wont be conflicts here,
      because of the way the nfa was constructed (the nfa does not make contact with eof) *)
      If[
        pState ~ MemberQ ~ GrammarItem[Production[NonTerminal["\[CapitalSigma]'"], {oldStart}], 2],
        ACTION[pState, EOFToken[]] = ACCEPT
      ];

    ][#, mapper[#]] &,
    parserStates
  ];

  (* All others actions are errors (remember ACTION and GOTO may return Null) *)

  Block[
    {
      stack = {parserInitialState},
      tokens = Append[#, EOFToken[]], production = Null,
      treeHookSymbol = Unique[]
    },
    InitTreeHook[treeHookSymbol];
    While[True,
      Which[
        ACTION[First@stack, First@tokens] ~ MatchQ ~ SHIFT[__],
        stack = Prepend[stack, ACTION[First@stack, First@tokens] /. SHIFT[state_] :> state];
        OnShiftTreeHook[treeHookSymbol, First@tokens];
        tokens = Rest@tokens;
        ,
        ACTION[First@stack, First@tokens] ~ MatchQ ~ REDUCE[__],
        production = ACTION[First@stack, First@tokens] /. REDUCE[prod_] :> prod;
        OnReduceTreeHook[treeHookSymbol, production];
        stack = Drop[stack, Length@ProductionBody@production];
        stack = Prepend[stack, GOTO[First@stack, ProductionNonTerminal@production]];
        ,
        ACTION[First@stack, First@tokens] ~ MatchQ ~ ACCEPT,
        Break[];
        ,
        True,
        Throw[StringForm["Parse error on `1`", Style[First@tokens, Green]]]
      ]
    ];
    OnFinishTreeHook[treeHookSymbol]
  ] &
];

(* TreeHook helpers for building a parse tree in SLRParserFor *)

ClearAll[InitTreeHook, OnShiftTreeHook, OnReduceTreeHook, OnFinishTreeHook];

InitTreeHook[symbol_Symbol] := symbol["top"] = 1;

(* Brackets for beautiful TreeForm formatting *)
OnShiftTreeHook[symbol_Symbol, token_Token] := symbol["tree"][++symbol["top"]] = token[];

OnReduceTreeHook[symbol_Symbol, prod_Production] := Block[{subtrees},
  subtrees = Sequence @@ Map[
    symbol["tree"],
    Range[symbol["top"] - Length@ProductionBody@prod + 1, symbol["top"]]
  ];
  symbol["top"] -= Length@ProductionBody@prod - 1;
  symbol["tree"][symbol["top"]] = (ProductionNonTerminal@prod)[subtrees];
];

OnFinishTreeHook[symbol_Symbol] := symbol["tree"][symbol["top"]];

End[]; (* `Private` *)

EndPackage[];
