(* ::Package:: *)

(* ::Section::Initialization:: *)
(*Code*)


(* ::Input::Initialization:: *)
BeginPackage["GlobalThings`"];


(* ::Input::Initialization:: *)
UpAppend::usage =
    "Aliases: upa , uupa (respectively)

UpAppend[recipient,element] pushes element to list referenced by recipient, or initializes recipient to be a list containing element

UpAppend[recipient,element,upvalueOwner] does the same, associating the assignment to upvalueOwner" ;

ElementPattern::usage = "Alias \!\(\*SubscriptBox[\(\[Element]\), \(::\)]\)

ElementPattern[elem,list] checks whether elem is member of list or follows any pattern defined by members of the list";

Intersects::usage = "Intersects[list1,list2] checks if any elements collide";

Cartesian::usage = "Cartesian[list1,list2] return the cartesian product";

Diff::usage = "Diff[] to choose two notebooks for compare";

MatchRegexpQ::usage = "MatchRegexpQ[string,regexp] transforms regexp string into a RegularExpression object and sees if string matches it.

Accepts string not being a string, returning False in such a case";

Summarizable::usage = "Summarizable[obj]=True to activate summary box for the expression obj[_Association]";

MyTreeForm::usage = "MyTreeForm[expr] is better than the original because it spaces the nodes"


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Input::Initialization:: *)
Options[UpAppend] = {Deduplicate -> False, UpvalueSymbol -> NoSymbol};
SetAttributes[UpAppend, HoldAll];
UpAppend[a_, b_, OptionsPattern[]] := Block[
  {},
  If[
    !MatchQ[OptionValue[UpvalueSymbol], NoSymbol],
    Evaluate[OptionValue[UpvalueSymbol]] /: a = If[!MatchQ[a, List[__]], {b}, If[
      TrueQ[OptionValue[Deduplicate]], DeleteDuplicates[a ~ Append ~ b], a ~ Append ~ b]],
    a = If[!MatchQ[a, List[__]], {b}, If[
      TrueQ[OptionValue[Deduplicate]], DeleteDuplicates[a ~ Append ~ b], a ~ Append ~ b]]
  ]
]


(* ::Input::Initialization:: *)
a_\[Tilde]b_ := MatchQ[a, b];
Unprotect[Element];
a_ \[Element] b_List := MemberQ[b, a];
a_ ~ ElementPattern ~ b_List := a\[Element]b \[Or] True \[Element] Map[a\[Tilde]#&, b];
a_List ~ Intersects ~ b_List := True\[Element]Map[# ~ ElementPattern ~ b&, a];
a_List ~ Cartesian ~ b_List := DeleteDuplicates[Flatten[Outer[List, a, b], 1]];


(* ::Input::Initialization:: *)
Diff[] := CreateDocument[NotebookTools`NotebookDiff[]]


(* ::Input::Initialization:: *)
MatchRegexpQ[string_, regexp_] := TrueQ[StringQ[string]\[And]StringMatchQ[string, RegularExpression[regexp]]]


(* ::Input::Initialization:: *)
SummarizableQ[obj_] := BooleanQ[Summarizable[obj]]\[And]Summarizable[obj] == True;


(* ::Input::Initialization:: *)
$icon = Graphics[{Red, Disk[]}, ImageSize -> Dynamic[{(*this seems to be the standard icon size*)Automatic, 3.5 CurrentValue["FontCapHeight"] / AbsoluteCurrentValue[Magnification]}]];


(* ::Input::Initialization:: *)
MakeBoxes[obj_[asc_Association] /; SummarizableQ[obj], form : (StandardForm | TraditionalForm)] := Module[
  {above, below = {}},
  above = Map[{BoxForm`SummaryItem[{ToString[#] <> ": ", asc[#]}]}&, Keys@asc];
  BoxForm`ArrangeSummaryBox[
    ToString@obj, (*head*)
    obj[asc], (*interpretation*)
    $icon, (*icon,use None if not needed*)
    above, (*always shown content*)
    below, (*expandable content*)
    form,
    "Interpretable" -> True
  ]
]


(* ::Input::Initialization:: *)
MyTreeForm[list_] := Module[{rectOffset, fontSize, p, i},
  rectOffset = {.33, .1};
  fontSize = 9;
  p = Cases[Cases[MakeBoxes[TreeForm@list, StandardForm], TooltipBox[x__] :> x, 7, Heads -> True], TagBox[x__, y__] :> DisplayForm[First@{x}], Heads -> True];
  i = 0;
  TreeForm[list, VertexRenderingFunction -> ({White, EdgeForm[Black], Rectangle[#1 - rectOffset, #1 + rectOffset], Black, Tooltip[Text[Style[#2, fontSize], #1], p[[i++]]]}&)]
];


(* ::Input::Initialization:: *)
End[];
EndPackage[];
