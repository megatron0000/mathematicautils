(* ::Package:: *)

BeginPackage["LoadPackage`"];


LoadPackage::usage = "LoadPackage[packageName] evaluates all initialization
cells on notebook specified.

First it is searched on $UserBaseDirectory/Applications/ as a file. Then as a file
inside a folder with the same name";

BuildDependencies;


Begin["`Private`"];


JoinString[list_List] := StringDrop[Map[# <> "/"&, list] <> "", -1];


LoadPackage[packageName_] := Module[
  {base = $UserBaseDirectory <> "/Applications/", parts = StringSplit[packageName, "`"], joined, backupContext = $Context, backupContextPath = $ContextPath,
    uniqueContext = "LoadPackageUniqueContext" <> StringReplace[CreateUUID[] <> "`", "-" -> ""], error = Null},
  RegisterDependency[backupContext, packageName];
  joined = JoinString[parts];
  $Context = uniqueContext;
  error = Catch[Which[
    FileExistsQ[base <> joined <> ".nb"],
    NotebookEvaluate[base <> joined <> ".nb", EvaluationElements -> "InitializationCell"],
    FileExistsQ[base <> joined <> "/" <> Last[parts] <> ".nb"],
    NotebookEvaluate[base <> joined <> "/" <> Last[parts] <> ".nb", EvaluationElements -> "InitializationCell"],
    True,
    Throw["Package " <> joined <> " was not found. From: " <> backupContext, PackageNotFoundError]
  ];,
    _
  ];
  (* ClearAll @@ Names[uniqueContext<>"*"]; *)
  Remove @@ Names[uniqueContext <> "*"];
  $Context = backupContext;
  $ContextPath = If[error === Null, DeleteDuplicates[backupContextPath ~ Append ~ packageName], backupContextPath];
  If[Not[error === Null], Throw[error, OpaqueError]]
];


RegisterDependency[dependent_, dependency_] := Null;

MatchAnyQ[expr_, formList_] := MemberQ[Map[MatchQ[expr, #]&, formList], True];


LoadPackage[packageName_, ignores_] := Block[{graph = {}},
  RegisterDependency[dependent_, dependency_] := Block[{},
    If[dependent == "Global`", Return[]];
    If[MemberQ[graph, dependent -> dependency] \[Or] MatchAnyQ[dependent -> dependency, ignores], Return[]];
    AppendTo[graph, dependent -> dependency];
  ];
  LoadPackage[packageName];
  RegisterDependency[dependent_, dependency_] := Null;
  Graph[graph, VertexLabels -> "Name"]
];


End[];
EndPackage[];
