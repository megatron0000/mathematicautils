(* Public *)

BeginPackage["Automaton`NFAToDFA`"];

NFAToDFA::usage = "NFAToDFA[nfa] returns a DFA";

DFA::usage = "DFA[] returns a DFA[id] object";

Begin["`Private`"];

<< GlobalThings`;
<< Automaton`NFABuilder`;

(* Converter *)

NFAToDFA[nfa_NFA] := Block[
  {visited = {}, stack = {}, states, newNFA = NFA[],
    current = EpsilonClosure@StartState@nfa},
  MakeStart[newNFA, states[current] = NFAState[]];
  visited ~ UpAppend ~ current;
  stack ~ UpAppend ~ current;
  While[stack != {},
    current = stack[[-1]];
    stack = Delete[stack, -1];
    If[current ~ Intersects ~ AcceptStates[nfa],
      MakeAccepting[newNFA, states[current]]];
    Map[
      Block[{new = EpsilonClosure@TransitionEndpoints[current, #]},
        If[
          ! new \[Element] visited,
          states[new] = NFAState[];
          visited ~ UpAppend ~ new;
          stack ~ UpAppend ~ new;
        ];
        AddTransition[Transition[states[current], states[new], #]];
      ] &,
      TransitionChars[current]
    ]
  ];
  newNFA
];

End[];

EndPackage[];
