(* Public *)

BeginPackage["Automaton`AutomatonSimulator`"];

DFAMatchQ::usage =
    "DFAMatchQ[nfa,string] checks whether nfa accepts string. nfa must
    be a valid DFA, else inconsistent results may arise.";

Begin["`Private`"];

<< "GlobalThings`";
<< "Automaton`NFABuilder`";

DFAMatchQ[nfa_NFA, string_] := Block[
  {chars = StringSplit[string, ""], index = 1,
    current = StartState[nfa]},
  While[
    Not[current ~ MatchQ ~ {}] && index <= Length[chars],
    current = (If[Length@# == 0, {}, First@#] &)@TransitionEndpoints[current, chars[[index]]];
    index++;
  ];
  Not[current ~ MatchQ ~ {}] && current \[Element] AcceptStates[nfa] && index > Length[chars]
];

End[];
EndPackage[];
