(* ::Package:: *)

(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: NFABuilder *)
(* :Context: NFABuilder` *)
(* :Author: vitor *)
(* :Date: 2018-05-16 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["Automaton`NFABuilder`"];
(* Exported symbols added here with SymbolName::usage *)

NFAState::usage =
    "NFAState[] creates a NFAState[id], in which id is unique";

StartState::usage = "StartState[nfa] gets its unique start state";

AcceptStates::usage =
    "AcceptStates[nfa] gets its set of accepting states";

Transition::usage =
    "Transition[node1,node2,char] means node1 goes to node2 on char \
(which may well be a pattern)";

Transitions::usage =
    "Transitions[nfaState] returns a set of Transition objects";

NFA::usage =
    "NFA[] generates an NFA[id] object, id being unique for every \
call";

DrawNFA::usage = "DrawNFA[nfa]";

MakeStart::usage =
    "MakeStart[nfa,state] overwrites nfa so that state is the new start \
state";

MakeAccepting::usage =
    "MakeAccepting[nfa,state] appends state to the list of accepting \
states";

AddTransition::usage =
    "AddTransition[Transition[from,to,char]] creates a transition from \
from to to via char (which may be a pattern)";

EpsilonClosure::usage =
    "EpsilonClosure[state] returns list of states reachable from state \
via any number of \[Epsilon]-transitions (including none)

  EpsilonClosure[stateList] returns \[Union] of states from \
EpsilonClosure[state] for each state in stateList";

TransitionEndpoints::usage =
    "TransitionEndpoints[state] returns set of states reachable from \
state via 1 non-\[Epsilon] transition

  TransitionEndpoints[stateList] returns set of states reachable from \
any state in stateList via 1 non-\[Epsilon] transition

  TransitionEndpoints[state,char] returns set of states reachable \
from state via 1 char-transition

  TransitionEndpoints[stateList,char] returns set of states reachable \
from any state in stateList via 1 char-transition";

TransitionChars::usage =
    "TransitionChars[state] returns all chars (except \[Epsilon]) which \
belong to any transition whose origin is state

  TransitionChars[stateList] returns all chars (except \[Epsilon]) \
which belong to any transition whose origin is any state in \
stateList";

ListStates::usage=
	"ListStates[nfa] collects all states from nfa in a list";

RemoveEpsilon::usage = 
	"RemoveEpsilon[nfa] removes all \[Epsilon]-transitions from the NFA without altering its language. 
	Does not alter the original nfa; returns a new one";

CloneNFA::usage = 
  "CloneNFA[nfa] clones the states and optionally sets start and end states. 'SetStart'->True and 'SetAccepts'->True
  are the default option values (note: an NFA without start is useless). 'SetAccepts' can be a function, in which case it
  will iterate through the original nfa's states, and will make the cloned state an accepting one if the function returns
  True";

Begin["`Private`"];

<< GlobalThings`;

NFAState[] := NFAState[Unique[]];
NFA[] := NFA[Unique[]];

Transitions[any_] := {};

AddTransition[Transition[node1_NFAState, node2_NFAState, char_]] := Block[
  {},
  UpAppend[
    Transitions[node1],
    Transition[node1, node2, char],
    Deduplicate -> True,
    UpvalueSymbol -> NFAState
  ]
];

MakeAccepting[nfa_NFA, state_NFAState] := UpAppend[
  AcceptStates[nfa],
  state,
  Deduplicate -> True,
  UpvalueSymbol -> NFA
];

MakeStart[nfa_NFA, state_NFAState] := NFA /: StartState[nfa] = state;

(* Helpers for dismantling Transition objects *)

ClearAll[Origin, Endpoint, Char];
Origin[Transition[origin_, other_, char_]] := origin;
Endpoint[Transition[origin_, other_, char_]] := other;
Char[Transition[origin_, other_, char_]] := char;


(* Transition facilitators for sets of states *)

ClearAll[EpsilonEndpoints, EpsilonClosureHelper];

EpsilonEndpoints[state_NFAState] :=
    Endpoint /@
        Select[Transitions[state], Char[#] ~ MatchQ ~ "\[Epsilon]" &];

EpsilonClosureHelper[done_List, new_List] := Which[
  new == {}, done,
  new != {},
  EpsilonClosureHelper[done \[Union] new,
    Select[DeleteDuplicates@
        Flatten@Map[EpsilonEndpoints,
          new], ! (# \[Element] done \[Union] new) &]]
];

EpsilonClosure[state_NFAState] := EpsilonClosureHelper[{}, {state}];

EpsilonClosure[stateList_] :=
    DeleteDuplicates@Flatten@Map[EpsilonClosure, stateList];


(* Note we check char ~ MatchQ ~ Char[#]. This is because
transition chars from the NFA can be patterns *)

TransitionEndpoints[state_NFAState, char_] :=
    Endpoint /@ Select[Transitions[state], char ~ MatchQ ~ Char[#] &];

TransitionEndpoints[stateList_, char_] :=
    DeleteDuplicates[Map[TransitionEndpoints[#, char] &, stateList]];

TransitionEndpoints[state_NFAState] :=
    DeleteDuplicates@Flatten@Map[TransitionEndpoints[state, #] &, #] &@
        Select[#, Not[# ~ MatchQ ~ "\[Epsilon]"] &] &@
        Map[Char, Transitions[state]];

TransitionEndpoints[stateList_] :=
    DeleteDuplicates@Flatten@Map[TransitionEndpoints, stateList];

TransitionChars[state_NFAState] :=
    DeleteDuplicates@Select[#, Not[# ~ MatchQ ~ "\[Epsilon]"] &] &@
        Map[Char, Transitions[state]];

TransitionChars[stateList_] :=
    DeleteDuplicates@Flatten@Map[TransitionChars, stateList];


(* Gather all states of an NFA *)

ListStates[nfa_NFA] := FixedPoint[EpsilonClosure@# ~ Union ~ TransitionEndpoints@# &, {StartState@nfa}]


(* Remove all epsilon-transitions from an NFA, adding other transitions at the same time, so as to
	keep the NFA language intact *)

RemoveEpsilon[nfa_NFA]:=Block[{new=NFA[],newState,old2new=Association@@(#->NFAState[]&/@ListStates@nfa)},
	Scan[
		Function[oldState,
			newState=old2new@oldState;
			If[oldState===StartState@nfa,MakeStart[new,newState]];
			If[IntersectingQ[EpsilonClosure@oldState,AcceptStates@nfa],MakeAccepting[new,newState]];
			AddTransition/@ReplaceAll[
				Transition[_,end_,char_]:>Transition[newState,old2new@end,char]
			]@DeleteCases[Transition[_,_,"\[Epsilon]"]]@Flatten@Map[Transitions,EpsilonClosure@oldState];
			Map[Map[
				Function[endpoint,AddTransition@Transition[newState,old2new@endpoint,#]]
			]@EpsilonClosure@TransitionEndpoints[oldState,#]& ]@TransitionChars[oldState]
		],
	ListStates[nfa]
	];
	new
];


(* Nice-to-have drawing util *)

DrawNFA[nfa_NFA] := Block[
  {vertexNumber = 1, edges, visited = {}, stack = {StartState[nfa]},
    Pop, current, Pair, State, Char, NumberOf},
  Pop[list_] :=
      Block[{popped = list[[-1]]}, list = Delete[list, -1]; popped];
  SetAttributes[Pop, HoldAll];
  Pair[state_, char_][State] := state;
  Pair[state_, char_][Char] := char;
  While[stack != {},
    visited ~ UpAppend ~ (current = Pop@stack);
    NumberOf[current] = vertexNumber;
    Map[
      Function[
        {pair},
        UpAppend[
          edges[current -> pair[State]],
          pair[Char],
          Deduplicate -> True
        ];
        If[! ((visited \[Union] stack) ~ MemberQ ~ pair[State]),
          stack ~ UpAppend ~ pair[State]]
      ],
      Transitions[
        current] /. {Transition[curr_, other_, char_] :>
          Pair[other, char]}
    ];
    vertexNumber++;
  ];
  Graph[Map[
    Labeled[#[[1, 1, 1]],
      If[Length[#[[2]]] >= 6,
        Join[#[[2, 1 ;; 5]], {"\[Ellipsis]"}], #[[2]]]] &,
    DownValues[edges]],
    VertexLabels ->
        Map[(# ->
            ToString[NumberOf[#]] <>
                If[TrueQ[StartState[nfa] == #], ":Start", ""] <>
                If[MemberQ[AcceptStates[nfa], #], ":Accepting", ""]) &,
          visited]]
];

Options[CloneNFA] = {"SetStart" -> True, "SetAccepts" -> True};
CloneNFA[nfa_NFA, OptionsPattern[]] := With[{
    new = NFA[],
    old2new = Association @@ Map[#->NFAState[]&]@ListStates@nfa
  },
  Scan[AddTransition@Transition[old2new@#[[1]], old2new@#[[2]], #[[3]] ]& , Flatten[Transitions/@ListStates@nfa]];
  If[OptionValue@"SetStart" === True, MakeStart[new, old2new@StartState@nfa]];
  Switch[
   OptionValue@"SetAccepts",
   True,
   MakeAccepting[new, old2new@#]& /@ AcceptStates@nfa,
   False,
   Null,
   _, (* is a function !*)
   MakeAccepting[new, old2new@#]& /@ Select[ListStates@nfa, OptionValue@"SetAccepts"]
  ];

  new
];


End[]; (* `Private` *)

EndPackage[];
