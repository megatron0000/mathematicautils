(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: StringToGrammar *)
(* :Context: StringToGrammar` *)
(* :Author: vitor *)
(* :Date: 2018-05-22 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["CFGrammar`StringToGrammar`"];
(* Exported symbols added here with SymbolName::usage *)


StringToGrammar::usage =
    "StringToGrammar[string] builds a working Grammar (context free) \
from string specification.

  CAUTION with token literals. An 'x' generates Token[x,x] in the \
output Grammar (which will enforce the lexeme, and the token name may \
collide with other non-literal tokens)";


Begin["`Private`"];

<< LexerGenerator`;
<< SLR`;
<< CFGrammar`;

(* Function to understand the tokens used by a person
when writing his/her grammar with our syntax *)

ClearAll[Tokenize];
Tokenize =
    BuildTokenizer@
        ReadString@FileNameJoin@{DirectoryName[$InputFileName], "lexicon.txt"};

(* Function to build a syntax tree of the person's written grammar *)

ClearAll[Parse];
Parse = Module[{grammar = Grammar[], parser},
(* This is the specification of the syntax a person needs to use
for writing the grammar he/she wants to transform into a Grammar object *)

  MakeStartSymbol[grammar, NonTerminal["Start"]];
  AddProduction[grammar, Production[First@#, Last@#]] & /@ {
    NonTerminal["Start"] -> {NonTerminal["ProductionList"]},
    NonTerminal["ProductionList"] -> {NonTerminal["Production"]},
    NonTerminal["ProductionList"] -> {NonTerminal["Production"],
      Token["feed", _]},
    NonTerminal["ProductionList"] -> {NonTerminal["Production"],
      Token["feed", _], NonTerminal["ProductionList"]},
    NonTerminal["Production"] -> {Token["nonTerminal", _],
      Token["arrow", _], NonTerminal["BodyList"]},
    NonTerminal["BodyList"] -> {NonTerminal["Body"]},
    NonTerminal["BodyList"] -> {NonTerminal["Body"],
      Token["alternation", _], NonTerminal["BodyList"]},
    NonTerminal["Body"] -> {NonTerminal["BodyAtom"]},
    NonTerminal["Body"] -> {NonTerminal["BodyAtom"],
      NonTerminal["Body"]},
    NonTerminal["BodyAtom"] -> {Token["token", _]},
    NonTerminal["BodyAtom"] -> {Token["tokenLiteral", _]},
    NonTerminal["BodyAtom"] -> {Token["nonTerminal", _]}
  };
  parser = SLRParserFor[grammar];
  Function[string,
    parser@(Cases[Tokenize@string, Except@Token["ws", _]] //. {
      {Token["feed", _], rest___} :> {rest}, {rest___, Token["feed", _]} :> {rest}
    } /. {Token["tokenLiteral", a_] :> Token["tokenLiteral", StringJoin@StringPart[a, 2;;-2]]})
  ]
];

(* Function to turn the tree built by Parse "upside down".
It removes our terminology (Start, BodyList, BodyAtom, etc)  and
inserts  the Tokens and NonTerminals the person wants, which leads to a new syntax tree *)

ClearAll[ToUserIdiom];
ToUserIdiom[tree : NonTerminal["Start"][__]] :=
    (tree
        //. {NonTerminal["ProductionList"][children__] :> children}
        //. {a_[b___, Token["feed", _][] | Token["arrow", _][] | Token["alternation", _][], c___]
        :> a[b, c]}
        //. {NonTerminal["BodyList" | "BodyAtom"][a__] :> a}
        //. {NonTerminal["Production"][head_[], others__] :> head[others]}
        //. {NonTerminal["Body"][a__, NonTerminal["Body"][b__]] :> NonTerminal["Body"][a, b]}
        //. {NonTerminal["Start"][a___, Token["nonTerminal", b_][d__], c___, Token["nonTerminal", b_][e__], f___]
        :> NonTerminal["Start"][a, Token["nonTerminal", b][d, e], c, f]}
        //. {NonTerminal["Body"][a__] :> List[a]}
        //. {NonTerminal["Start"][a__] :> "root"[a]}
        //. {Token["nonTerminal", a_] :> NonTerminal[a], Token["token", a_] :> Token[a, _], Token["tokenLiteral", a_]
        :> Token[a, a]}
    );

StringToGrammar[source_String] := Module[{grammar = Grammar[]},
  Scan[
    Function[productionBranch,
      AddProduction[grammar, Production[Head@productionBranch, #]] & /@
          productionBranch
    ],
    (MakeStartSymbol[grammar, Head@First@#]; #) &@
        ToUserIdiom@Parse@source //. {a_[b__][] :> a[b]}
  ];
  grammar
];

End[]; (* `Private` *)

EndPackage[];
