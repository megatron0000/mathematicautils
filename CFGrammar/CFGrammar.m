(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: CFGrammar *)
(* :Context: CFGrammar` *)
(* :Author: vitor *)
(* :Date: 2018-05-21 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["CFGrammar`"];
(* Exported symbols added here with SymbolName::usage *)

(* Grammar symbols*)

Token::usage = "Token[name,lexeme] represents a token";

TokenName::usage = "TokenName[token]";

TokenLexeme::usage = "TokenLexeme[token]";

EpsilonToken::usage = "EpsilonToken[] represents the empty token";

EOFToken::usage = "EOFToken[] represents the end of file";

NonTerminal::usage = "NonTerminal[name] represents a non terminal

  NonTerminal[production] retrieves the production's head
  ";

NonTerminalName::usage = "NonTerminalName[nonTerminal]";

(* grammar structures *)

Production::usage =
    "Production[nonTerminal, body] represents nonTerminal->body in a \
grammar. body is a list of Token and NonTerminal";

ProductionBody::usage =
    "ProductionBody[production] retrieves the production's body, which \
is a list";

ProductionNonTerminal::usage = "ProductionNonTerminal[production]";

(* "Grammar" object *)

Grammar::usage =
    "Grammar[] produces Grammar[uuid], which represents a CFG";

Productions::usage =
    "Productions[grammar, nonTerminal] returns a list of Production

    Productions[grammar] returns all productions from all non-terminals
    ";

NonTerminals::usage =
    "NonTerminals[grammar] returns a list of NonTerminal";

AddProduction::usage =
    "DEPRECATED (Invalidates FIRST and FOLLOW cached values)

  AddProduction[grammar, production] adds it to the grammar, if not \
already present";

MakeStartSymbol::usage = "MakeStartSymbol[grammar, nonTerminal]";

StartSymbol::usage = "StartSymbol[grammar]. Is Null if not set";

ComputeFunctions::usage =
    "ComputeFunctions[grammar] recomputes FIRST and FOLLOW operations \
for said grammar, a necessary operation when it is altered by calls \
to AddProduction and MakeStartSymbol";

FIRST::usage =
    "FIRST[grammar, symbol] returns a list of grammar symbols. 'symbol' \
is either a Token or a NonTerminal

  FIRST[grammar, sentential] expects 'sentential' to be a list of \
grammar symbols (Token or NonTerminal).
  ";

FOLLOW::usage =
    "FOLLOW[grammar, symbol] returns a list of grammar symbols. \
'symbol' is either a Token or a NonTerminal

  FOLLOW[grammar, sentential] expects 'sentential' to be a list of \
grammar symbols (Token or NonTerminal).
  ";

CloneGrammar::usage =
    "CloneGrammar[grammar] returns an independent copy (modifications \
to one do not affect the other)";

Begin["`Private`"];

<< GlobalThings`;

(* Grammar symbols *)

Token /: TokenName[Token[name_, lexeme_]] := name;

Token /: TokenLexeme[Token[name_, lexeme_]] := lexeme;

NonTerminal /: NonTerminalName[NonTerminal[name_]] := name;

EpsilonToken[] = Token["\[Epsilon]", Null];

EOFToken[] = Token["\[EmptySet]EOF", Null];

ProductionNonTerminal[Production[nonTerminal_, body_]] := nonTerminal;

ProductionBody[Production[nonTerminal_, body_]] := body;

(* Grammar object *)

(** Internal helper **)

ClearAll[Modify];

SetAttributes[Modify, HoldAll];
Modify[symbol_, newValues_] :=
    If[Not[Sort@Union[symbol, newValues] === Sort[symbol]],
      Grammar /: symbol = Union[symbol, newValues]; True, False];

(** Public parts **)

Grammar /: Grammar[] := Grammar[Unique[]];

Grammar /: Grammar[stringGrammar_String] := Block[{},
  Null
];

Grammar /: Productions[self_Grammar, nonTerminal_NonTerminal] :=
    Productions[self, nonTerminal] = {};

Grammar /: Productions[self_Grammar] := 
    Flatten[Productions[self, #]& /@ NonTerminals[self], 1]

Grammar /: NonTerminals[self_Grammar] := NonTerminals[self] = {};

AddProduction[self_Grammar, production_Production] := Block[{},
  UpAppend[
    Productions[self, ProductionNonTerminal[production]],
    production,
    UpvalueSymbol -> Grammar,
    Deduplicate -> True
  ];
  Scan[UpAppend[NonTerminals[self], #, UpvalueSymbol -> Grammar, Deduplicate -> True]&,
    Select[ProductionBody[production], # ~ MatchQ ~ NonTerminal[__] &] ~
        Append ~ ProductionNonTerminal[production]
  ];
];

Grammar /: MakeStartSymbol[self_Grammar, nonTerminal_NonTerminal] :=
    Grammar /: StartSymbol[self] = nonTerminal;

Grammar /: StartSymbol[self_Grammar] :=
    Grammar /: StartSymbol[self] = Null;

Grammar /: ComputeFunctions[self_Grammar] :=
    Module[{modified = True, productions, backup},

      productions =
          Flatten[Map[Productions[self, #] &, NonTerminals[self]], 1];

      Grammar /: FIRST[self, token_Token] := {token};
      Scan[(Grammar /: FIRST[self, #] = {}) &, NonTerminals[self]];

      While[modified,
        modified = False;
        Scan[
          Function[{nonTerminal, body},
            backup = FIRST[self, nonTerminal];
            NestWhile[
              Function[symbolList,
                Grammar /: FIRST[self, nonTerminal] =
                    Union[FIRST[self, nonTerminal],
                      FIRST[self, First@symbolList]]; Rest@symbolList],
              body,
              # ===
                  body \[Or] (Length[#] != 0 \[And] EpsilonToken[] \[Element] FIRST[self, body[[-Length[#] - 1]]]) &
            ];
            If[And@Map[EpsilonToken[] \[Element] FIRST[self, #] &, body],
              UpAppend[
                FIRST[self, nonTerminal],
                EpsilonToken[],
                UpvalueSymbol -> Grammar,
                Deduplicate -> True
              ]];
            If[! SameQ[Sort@backup, Sort@FIRST[self, nonTerminal] ],
              modified = True];
          ][ProductionNonTerminal@#, ProductionBody@#]&,
          productions
        ];
      ];

      Scan[(Grammar /: FOLLOW[self, #] = {}) &, NonTerminals[self]];

      UpAppend[
        FOLLOW[self, StartSymbol[self]],
        EOFToken[],
        UpvalueSymbol -> Grammar,
        Deduplicate -> True
      ];

      modified = True;
      While[modified,
        modified = False;
        Scan[
          Function[{nonTerminal, body},
            Block[{rest = body},
              For[Null, Length@rest > 1, rest = Rest@rest,
                If[First@rest \[Tilde] Token[__], Continue[]];

                modified =
                    modified \[Or]
                Modify[FOLLOW[self, First@rest],
                  Cases[FIRST[self, Rest@rest], Except[EpsilonToken[]]]];

                If[EpsilonToken[] \[Element] FIRST[self, Rest@rest],
                  modified =
                      modified \[Or]
                  Modify[FOLLOW[self, First@rest],
                    FOLLOW[self, nonTerminal]]];
              ];

              If[First@rest \[Tilde] NonTerminal[__],
                modified =
                    modified \[Or]
                Modify[FOLLOW[self, First@rest],
                  FOLLOW[self, nonTerminal]]];
            ];
          ][ProductionNonTerminal@#, ProductionBody@#] &,
          productions
        ];
      ];
    ];

Grammar /: FIRST[self_Grammar, sentential_List] :=
    DeleteDuplicates@Flatten@Last@Reap[Block[{rest = sentential},
      Sow /@ FIRST[self, First@sentential];
      While[
        Length@rest > 1 \[And]
        EpsilonToken[] \[Element] FIRST[self, First@rest],
        rest = Rest@rest; Sow /@ FIRST[self, First@rest];
      ];
      If[
        Length@rest === 1 \[And]
        EpsilonToken[] \[Element] FIRST[self, First@rest],
        Sow[EpsilonToken[]]];
    ]];

Grammar /: CloneGrammar[self_Grammar] :=
    Block[{newGrammar = Grammar[]},
      MakeStartSymbol[newGrammar, StartSymbol@self];
      Map[AddProduction[newGrammar, #] &,
        Flatten@Map[Productions[self, #] &, NonTerminals@self ] ];
      ComputeFunctions[newGrammar]; newGrammar ];

End[]; (* `Private` *)

EndPackage[];
