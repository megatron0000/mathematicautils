(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: LexerGenerator *)
(* :Context: LexerGenerator` *)
(* :Author: vitor *)
(* :Date: 2018-05-22 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["LexerGenerator`"];
(* Exported symbols added here with SymbolName::usage *)

BuildTokenizer::usage =
    "BuildTokenizer[string] uses string (a collection of variable->regexp) to generate a Tokenizer[nfa] object";

Tokenizer::usage =
    "Tokenizer[nfa] is an operator. Tokenizer[nfa][string] tokenizes string.
    Only works for Tokenizers built by BuildTokenizer";

Begin["`Private`"];

<< GlobalThings`;
<< RegexpToNFA`;
<< Automaton`NFABuilder`;
<< CFGrammar`;
<< LexerGenerator`RegexpCollectionLexer`;

Summarizable[Parser] = True;

ClearAll[Parser];
Parser[string_?StringQ] := Block[
  {parser =
      Parser@<|"tokens" -> Tokenize[string], "id" -> Unique[]|>},
  Parser /: TokenList[parser] = Tokenize[string];
  Parser /: Index[parser] = 1;
  Parser /: PriorityCount[parser] = 0;
  Parser /: Priority[p_Parser] :=
      Parser /: PriorityCount[p] = PriorityCount[p] + 1;
  Parser /: RegexpToNonTerminal[parser, reg_] = Null;
  Parser /: CollectionNFA[parser] = NFA[];
  MakeStart[CollectionNFA[parser], NFAState[]];
  parser
];

Parser /: Lookahead[parser_Parser, n_] := If[
  Index[parser] + n - 1 > Length@TokenList[parser],
  Token[EOF[Index[parser] + n - 1 - Length@TokenList[parser] ],
    Null],
  TokenList[parser][[Index[parser] + n - 1]]
];

Parser /: Match[parser_Parser, token_Token] := If[
  Lookahead[parser, 1] ~ MatchQ ~ token,
  Parser /: Index[parser] = Index[parser] + 1; Lookahead[parser, 0],
  Throw[
    StringForm["Expected `1`. Got `2`", Style[token, Green] , Style[Lookahead[parser, 1], Green]] ,
    UnexpectedTokenError
  ]
];

Parser /: ProductionRule[parser_Parser] := Block[
  {word , regexp, nfaBranch},
  word = Match[parser, Token["word", _]] /.
      Token["word", lexeme_] :> lexeme;
  Match[parser, Token["arrow", _]];
  regexp =
      Match[parser, Token["string", _]] /. Token[_, lexeme_] :> lexeme;
  Parser /: Priority[parser, regexp] = Priority[parser];
  If[
    StringQ[RegexpToNonTerminal[parser, regexp]],
    Throw[
      StringForm[
        "Pattern `1` is already associated to `2`",
        Style[regexp, Green] ,
        Style[RegexpToNonTerminal[parser, regexp], Green]
      ],
      AssociatedRegexpError
    ],
    Parser /: RegexpToNonTerminal[parser, regexp] = word
  ];
  (* Populate NFA and branch *)
  nfaBranch = RegexpToNFA[regexp];

  If[DFAMatch[nfaBranch, "\[Epsilon]"],
    Throw[
      StringForm["\[Epsilon]-generating regexps are forbidden: `1`", regexp],
      ForbiddenRegexpError
    ]
  ];

  AddTransition[
    Transition[StartState@CollectionNFA[parser], StartState@nfaBranch,
      "\[Epsilon]"]];
  Map[MakeAccepting[CollectionNFA[parser], #] &,
    AcceptStates@nfaBranch];
  Map[(Metadata[#] = regexp) &, AcceptStates@nfaBranch];
];

Parser /: TopDownParse[parser_Parser] := Block[
  {},
  If[
    Lookahead[parser, 1] ~ MatchQ ~ Token["linefeed", _],
    Match[parser, Token["linefeed", _]]
  ];
  While[
    Not[ Lookahead[parser, 1] ~ MatchQ ~ Token[EOF[_], Null] ],
    Which[
      Lookahead[parser, 5] ~ MatchQ ~ Token["word", _],
      ProductionRule[parser]; Match[parser, Token["linefeed", _]],
      Lookahead[parser, 4] ~ MatchQ ~ Token["linefeed", _],
      ProductionRule[parser]; Match[parser, Token["linefeed", _]],
      True,
      Throw["Expected production or productionList separated by \\n"]
    ]
  ];
  CollectionNFA[parser]
];

BuildTokenizer[collection_?StringQ] := Module[
  {parser = Parser[collection], nfa},
  nfa = TopDownParse[parser];
  Tokenizer[nfa][sourceString_?StringQ] := Block[
    {stack, index = 1, lastMatchIndex = 1,
      source = StringSplit[sourceString, ""], nonTerminal, tokens,
      lastAccept},
    While[
      index <= Length@source,

      stack = {EpsilonClosure[StartState[nfa]]};
      While[
      (* Use ElementPattern
       to allow transition chars to act as patterns (required to
       implement "." metacharacter of regexps, for example) *)
        index <= Length@source \[And] source[[index]] ~ ElementPattern ~ TransitionChars@Last@stack,
        stack ~ AppendTo ~ EpsilonClosure@TransitionEndpoints[Last@stack, source[[index++]]]
      ];

      If[index == lastMatchIndex,
        Throw[
          StringForm["Ungrammatical string at position `1`. Character `2`", index, If[index <= Length[source], source[[index]], "string end"] ],
          UngrammaticalStringError
        ]
      ];

      {nonTerminal, {{lastAccept}}} = Reap[
        Last@Flatten@Extract[#, Position[#, {0, _} ] ] &@
            Transpose[{Transpose[#][[1]] - Min[Transpose[#][[1]] ], Transpose[#][[2]]}] &@
            Map[{Priority[parser, #], RegexpToNonTerminal[parser, #1]} &, #] &@
            Select[#, StringQ] &@
            Map[Metadata, #] &@
            Sow[
              Last[
                Select[stack, # ~ Intersects ~ AcceptStates[nfa] & ],
                Throw[
                  StringForm["Ungrammatical string at position `1`. Character `2`", index, If[index <= Length[source], source[[index]], "string end"] ],
                  UngrammaticalStringError
                ]
              ]
            ]
      ];

      (* bugfix: Last position,
      not first (last position means longer matched string) *)

      index = index - (Length[stack] -
          Last@Flatten@Position[stack, lastAccept]);

      tokens ~ UpAppend ~ Token[
        nonTerminal,
        StringJoin[source[[lastMatchIndex ;; index - 1]]]
      ];
      lastMatchIndex = index;
    ];

    tokens
  ];
  Tokenizer[nfa]
];

End[]; (* `Private` *)

EndPackage[];
