(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     33711,        844]
NotebookOptionsPosition[     27227,        697]
NotebookOutlinePosition[     31227,        783]
CellTagsIndexPosition[     31184,        780]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"BeginPackage", "[", 
   RowBox[{"\"\<LexerGenerator`\>\"", ",", 
    RowBox[{"{", "\"\<LoadPackage`\>\"", "}"}]}], "]"}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"a13b7e82-d940-4731-a11d-651909fe5a62"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"BuildTokenizer", "::", "usage"}], "=", 
   "\"\<BuildTokenizer[string] uses string (a collection of variable->regexp) \
to generate a Tokenizer[] object\>\""}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Tokenizer", "::", "usage"}], "=", 
   "\"\<Tokenizer[nfa] is an operator. Tokenizer[nfa][string] tokenizes \
string. Only works for Tokenizers built by BuildTokenizer\>\""}], 
  ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"85ec3a26-d5ba-4c14-9626-b3228652dd29"],

Cell[BoxData[
 TemplateBox[{
  "BuildTokenizer","shdw",
   "\"Symbol \\!\\(\\*RowBox[{\\\"\\\\\\\"BuildTokenizer\\\\\\\"\\\"}]\\) \
appears in multiple contexts \\!\\(\\*RowBox[{\\\"{\\\", \
RowBox[{\\\"\\\\\\\"LexerGenerator`\\\\\\\"\\\", \\\",\\\", \
\\\"\\\\\\\"Notebook$$16$389803`\\\\\\\"\\\"}], \\\"}\\\"}]\\); definitions \
in context \\!\\(\\*RowBox[{\\\"\\\\\\\"LexerGenerator`\\\\\\\"\\\"}]\\) may \
shadow or be shadowed by other definitions.\"",2,8,3,27395928269796127907,
   "Local","LexerGenerator`BuildTokenizer"},
  "MessageTemplate2"]], "Message", \
"MSG",ExpressionUUID->"da5959f6-e785-4ff0-8133-abc09d2f3b57"]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<GlobalThings`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
  "LoadPackage", "@", "\"\<LexerGenerator`RegexpCollectionLexer`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<RegexpToNFA`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<Automaton`NFAToDFA`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<Automaton`AutomatonSimulator`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<Automaton`NFABuilder`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<CFGrammar`\>\""}], ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"5969376e-9eef-493c-bef7-e7b8f4aa0f23"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"eff3517b-18cd-487a-ad96-a6ea2d12298e"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Summarizable", "[", "Parser", "]"}], "=", "True"}], 
  ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"57e33626-0b4a-49c8-90b2-15a492fa5a99"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Parser", "[", 
   RowBox[{"string_", "?", "StringQ"}], "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"parser", "=", 
      RowBox[{"Parser", "@", 
       RowBox[{"<|", 
        RowBox[{
         RowBox[{"\"\<tokens\>\"", "\[Rule]", 
          RowBox[{"Tokenize", "[", "string", "]"}]}], ",", 
         RowBox[{"\"\<id\>\"", "\[Rule]", 
          RowBox[{"CreateUUID", "[", "]"}]}]}], "|>"}]}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Parser", "/:", 
      RowBox[{"TokenList", "[", "parser", "]"}], "=", 
      RowBox[{"Tokenize", "[", "string", "]"}]}], ";", "\[IndentingNewLine]", 
     
     RowBox[{"Parser", "/:", 
      RowBox[{"Index", "[", "parser", "]"}], "=", "1"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Parser", "/:", 
      RowBox[{"PriorityCount", "[", "parser", "]"}], "=", "0"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Parser", "/:", 
      RowBox[{"Priority", "[", "p_Parser", "]"}], ":=", 
      RowBox[{"Parser", "/:", 
       RowBox[{"PriorityCount", "[", "p", "]"}], "=", 
       RowBox[{
        RowBox[{"PriorityCount", "[", "p", "]"}], "+", "1"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Parser", "/:", 
      RowBox[{"RegexpToNonTerminal", "[", 
       RowBox[{"parser", ",", "reg_"}], "]"}], "=", "Null"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Parser", "/:", 
      RowBox[{"CollectionNFA", "[", "parser", "]"}], "=", 
      RowBox[{"NFA", "[", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"MakeStart", "[", 
      RowBox[{
       RowBox[{"CollectionNFA", "[", "parser", "]"}], ",", 
       RowBox[{"NFAState", "[", "]"}]}], "]"}], ";", "\[IndentingNewLine]", 
     "parser"}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"b6d585a1-0b3d-4b97-8fb6-53d17c2f471e"],

Cell[BoxData[
 RowBox[{"Parser", "/:", 
  RowBox[{"Lookahead", "[", 
   RowBox[{"parser_Parser", ",", "n_"}], "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"Index", "[", "parser", "]"}], "+", "n", "-", "1"}], ">", 
     RowBox[{"Length", "@", 
      RowBox[{"TokenList", "[", "parser", "]"}]}]}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"Token", "[", 
     RowBox[{
      RowBox[{"EOF", "[", 
       RowBox[{
        RowBox[{"Index", "[", "parser", "]"}], "+", "n", "-", "1", "-", 
        RowBox[{"Length", "@", 
         RowBox[{"TokenList", "[", "parser", "]"}]}]}], " ", "]"}], ",", 
      "Null"}], "]"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"TokenList", "[", "parser", "]"}], "\[LeftDoubleBracket]", 
     RowBox[{
      RowBox[{"Index", "[", "parser", "]"}], "+", "n", "-", "1"}], 
     "\[RightDoubleBracket]"}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"4a42fb47-283c-426b-9a4a-3f3ddfdd554c"],

Cell[BoxData[
 RowBox[{"Parser", "/:", 
  RowBox[{"Match", "[", 
   RowBox[{"parser_Parser", ",", "token_Token"}], "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Lookahead", "[", 
      RowBox[{"parser", ",", "1"}], "]"}], "\[Tilde]", "token"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Parser", "/:", 
      RowBox[{"Index", "[", "parser", "]"}], "=", 
      RowBox[{
       RowBox[{"Index", "[", "parser", "]"}], "+", "1"}]}], ";", 
     RowBox[{"Lookahead", "[", 
      RowBox[{"parser", ",", "0"}], "]"}]}], ",", "\[IndentingNewLine]", 
    RowBox[{"Throw", "[", 
     RowBox[{
      RowBox[{"\"\<Expected \>\"", "<>", 
       RowBox[{"ToString", "@", "token"}], "<>", "\"\<. Got \>\"", "<>", 
       RowBox[{"ToString", "@", 
        RowBox[{"Lookahead", "[", 
         RowBox[{"parser", ",", "1"}], "]"}]}]}], " ", ",", 
      "UnexpectedTokenError"}], "]"}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"47ee3f8f-c530-4ee8-a186-63fd46ac3f82"],

Cell[BoxData[
 RowBox[{"Parser", "/:", 
  RowBox[{"ProductionRule", "[", "parser_Parser", "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"word", " ", ",", "regexp", ",", "nfaBranch"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"word", "=", 
      RowBox[{
       RowBox[{"Match", "[", 
        RowBox[{"parser", ",", 
         RowBox[{"Token", "[", 
          RowBox[{"\"\<word\>\"", ",", "_"}], "]"}]}], "]"}], "/.", 
       RowBox[{
        RowBox[{"Token", "[", 
         RowBox[{"\"\<word\>\"", ",", "lexeme_"}], "]"}], "\[RuleDelayed]", 
        "lexeme"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Match", "[", 
      RowBox[{"parser", ",", 
       RowBox[{"Token", "[", 
        RowBox[{"\"\<arrow\>\"", ",", "_"}], "]"}]}], "]"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"regexp", "=", 
      RowBox[{
       RowBox[{"Match", "[", 
        RowBox[{"parser", ",", 
         RowBox[{"Token", "[", 
          RowBox[{"\"\<string\>\"", ",", "_"}], "]"}]}], "]"}], "/.", 
       RowBox[{
        RowBox[{"Token", "[", 
         RowBox[{"_", ",", "lexeme_"}], "]"}], "\[RuleDelayed]", 
        "lexeme"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Parser", "/:", 
      RowBox[{"Priority", "[", 
       RowBox[{"parser", ",", "regexp"}], "]"}], "=", 
      RowBox[{"Priority", "[", "parser", "]"}]}], ";", "\[IndentingNewLine]", 
     
     RowBox[{"If", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"StringQ", "[", 
        RowBox[{"RegexpToNonTerminal", "[", 
         RowBox[{"parser", ",", "regexp"}], "]"}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Throw", "[", 
        RowBox[{
         RowBox[{
         "\"\<Pattern \>\"", "<>", "regexp", "<>", 
          "\"\< is already associated to \>\"", "<>", 
          RowBox[{"RegexpToNonTerminal", "[", 
           RowBox[{"parser", ",", "regexp"}], "]"}]}], ",", 
         "AssociatedRegexpError"}], "]"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Parser", "/:", 
        RowBox[{"RegexpToNonTerminal", "[", 
         RowBox[{"parser", ",", "regexp"}], "]"}], "=", "word"}]}], 
      "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Populate", " ", "NFA", " ", "and", " ", "branch"}], " ", 
      "*)"}], "\[IndentingNewLine]", 
     RowBox[{"nfaBranch", "=", 
      RowBox[{"RegexpToNFA", "[", "regexp", "]"}]}], ";", 
     "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{"DFAMatch", "[", 
        RowBox[{"nfaBranch", ",", "\"\<\[Epsilon]\>\""}], "]"}], ",", 
       RowBox[{"Throw", "[", 
        RowBox[{
         RowBox[{
         "\"\<\[Epsilon]-generating regexps are forbidden: \>\"", "<>", 
          "regexp"}], ",", "ForbiddenRegexpError"}], "]"}]}], "]"}], ";", 
     "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"AddTransition", "[", 
      RowBox[{"Transition", "[", 
       RowBox[{
        RowBox[{"StartState", "@", 
         RowBox[{"CollectionNFA", "[", "parser", "]"}]}], ",", 
        RowBox[{"StartState", "@", "nfaBranch"}], ",", "\"\<\[Epsilon]\>\""}],
        "]"}], "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"Map", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"MakeAccepting", "[", 
         RowBox[{
          RowBox[{"CollectionNFA", "[", "parser", "]"}], ",", "#"}], "]"}], 
        "&"}], ",", 
       RowBox[{"AcceptStates", "@", "nfaBranch"}]}], "]"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Map", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Metadata", "[", "#", "]"}], "=", "regexp"}], ")"}], "&"}], 
       ",", 
       RowBox[{"AcceptStates", "@", "nfaBranch"}]}], "]"}], ";"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"a424bca6-190f-4576-a3da-2d077f4fc0bb"],

Cell[BoxData[
 RowBox[{"Parser", "/:", 
  RowBox[{"TopDownParse", "[", "parser_Parser", "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"If", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"Lookahead", "[", 
         RowBox[{"parser", ",", "1"}], "]"}], "\[Tilde]", 
        RowBox[{"Token", "[", 
         RowBox[{"\"\<linefeed\>\"", ",", "_"}], "]"}]}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Match", "[", 
        RowBox[{"parser", ",", 
         RowBox[{"Token", "[", 
          RowBox[{"\"\<linefeed\>\"", ",", "_"}], "]"}]}], "]"}]}], 
      "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"While", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Lookahead", "[", 
          RowBox[{"parser", ",", "1"}], "]"}], "!"}], "\[Tilde]", 
        RowBox[{"Token", "[", 
         RowBox[{
          RowBox[{"EOF", "[", "_", "]"}], ",", "Null"}], "]"}]}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Which", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          RowBox[{"Lookahead", "[", 
           RowBox[{"parser", ",", "5"}], "]"}], "\[Tilde]", 
          RowBox[{"Token", "[", 
           RowBox[{"\"\<word\>\"", ",", "_"}], "]"}]}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"ProductionRule", "[", "parser", "]"}], ";", 
          RowBox[{"Match", "[", 
           RowBox[{"parser", ",", 
            RowBox[{"Token", "[", 
             RowBox[{"\"\<linefeed\>\"", ",", "_"}], "]"}]}], "]"}]}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Lookahead", "[", 
           RowBox[{"parser", ",", "4"}], "]"}], "\[Tilde]", 
          RowBox[{"Token", "[", 
           RowBox[{"\"\<linefeed\>\"", ",", "_"}], "]"}]}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"ProductionRule", "[", "parser", "]"}], ";", 
          RowBox[{"Match", "[", 
           RowBox[{"parser", ",", 
            RowBox[{"Token", "[", 
             RowBox[{"\"\<linefeed\>\"", ",", "_"}], "]"}]}], "]"}]}], ",", 
         "\n", "\t", "True", ",", "\[IndentingNewLine]", 
         RowBox[{
         "Throw", "[", 
          "\"\<Expected production or productionList separated by \\\\n\>\"", 
          "]"}]}], "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
      "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"CollectionNFA", "[", "parser", "]"}]}]}], "\[IndentingNewLine]",
    "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"2346b4de-9915-4eac-b906-c8b14df68b55"],

Cell[BoxData[
 RowBox[{
  RowBox[{"BuildTokenizer", "[", 
   RowBox[{"collection_", "?", "StringQ"}], "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"parser", "=", 
       RowBox[{"Parser", "[", "collection", "]"}]}], ",", "nfa"}], "}"}], ",",
     "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"nfa", "=", 
      RowBox[{"TopDownParse", "[", "parser", "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"Tokenizer", "[", "nfa", "]"}], "[", 
       RowBox[{"sourceString_", "?", "StringQ"}], "]"}], ":=", 
      RowBox[{"Block", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"stack", ",", 
          RowBox[{"index", "=", "1"}], ",", 
          RowBox[{"lastMatchIndex", "=", "1"}], ",", 
          RowBox[{"source", "=", 
           RowBox[{"StringSplit", "[", 
            RowBox[{"sourceString", ",", "\"\<\>\""}], "]"}]}], ",", 
          "nonTerminal", ",", "tokens", ",", "lastAccept"}], "}"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"While", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"index", "\[LessEqual]", 
            RowBox[{"Length", "@", "source"}]}], ",", "\[IndentingNewLine]", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"stack", "=", 
             RowBox[{"{", 
              RowBox[{"EpsilonClosure", "[", 
               RowBox[{"StartState", "[", "nfa", "]"}], "]"}], "}"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"While", "[", "\[IndentingNewLine]", 
             RowBox[{"(*", " ", 
              RowBox[{"Use", " ", 
               SubscriptBox["\[Epsilon]", "::"], " ", "to", " ", "allow", " ",
                "transition", " ", "chars", " ", "to", " ", "act", " ", "as", 
               " ", "patterns", " ", 
               RowBox[{"(", 
                RowBox[{
                 RowBox[{
                 "required", " ", "to", " ", "implement", " ", "\"\<.\>\"", 
                  " ", "metacharacter", " ", "of", " ", "regexps"}], ",", " ", 
                 RowBox[{"for", " ", "example"}]}], ")"}]}], " ", "*)"}], 
             "\[IndentingNewLine]", 
             RowBox[{
              RowBox[{
               RowBox[{"index", "\[LessEqual]", 
                RowBox[{"Length", "@", "source"}]}], "\[And]", 
               RowBox[{
                RowBox[{
                "source", "\[LeftDoubleBracket]", "index", 
                 "\[RightDoubleBracket]"}], 
                SubscriptBox["\[Element]", "::"], 
                RowBox[{"TransitionChars", "@", 
                 RowBox[{"Last", "@", "stack"}]}]}]}], ",", 
              "\[IndentingNewLine]", 
              RowBox[{"stack", "\[LeftArrow]", 
               RowBox[{"EpsilonClosure", "@", 
                RowBox[{"TransitionEndpoints", "[", 
                 RowBox[{
                  RowBox[{"Last", "@", "stack"}], ",", 
                  RowBox[{"source", "\[LeftDoubleBracket]", 
                   RowBox[{"index", "++"}], "\[RightDoubleBracket]"}]}], 
                 "]"}]}]}]}], "\[IndentingNewLine]", "]"}], ";", 
            "\[IndentingNewLine]", "\[IndentingNewLine]", 
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{"index", "\[Equal]", "lastMatchIndex"}], ",", 
              RowBox[{"Throw", "[", 
               RowBox[{
                RowBox[{"\"\<Ungrammatical string at position \>\"", "<>", 
                 RowBox[{"ToString", "@", "index"}], "<>", 
                 "\"\<. Character \>\"", "<>", 
                 RowBox[{
                 "source", "\[LeftDoubleBracket]", "index", 
                  "\[RightDoubleBracket]"}]}], ",", 
                "UngrammaticalStringError"}], "]"}]}], "]"}], ";", 
            "\[IndentingNewLine]", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"{", 
              RowBox[{"nonTerminal", ",", 
               RowBox[{"{", 
                RowBox[{"{", "lastAccept", "}"}], "}"}]}], "}"}], "=", 
             RowBox[{"Reap", "[", "\[IndentingNewLine]", 
              RowBox[{
               RowBox[{
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{"Last", "@", 
                    RowBox[{"Flatten", "@", 
                    RowBox[{"Extract", "[", 
                    RowBox[{"#", ",", 
                    RowBox[{"Position", "[", 
                    RowBox[{"#", ",", 
                    RowBox[{"{", 
                    RowBox[{"0", ",", "_"}], "}"}]}], " ", "]"}]}], " ", 
                    "]"}]}]}], "&"}], "@", "\[IndentingNewLine]", 
                    RowBox[{"Transpose", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{"Transpose", "[", "#", "]"}], 
                    "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
                    "-", 
                    RowBox[{"Min", "[", 
                    RowBox[{
                    RowBox[{"Transpose", "[", "#", "]"}], 
                    "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
                    " ", "]"}]}], ",", 
                    RowBox[{
                    RowBox[{"Transpose", "[", "#", "]"}], 
                    "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}]}], 
                    "}"}], "]"}]}], "&"}], "@", "\[IndentingNewLine]", 
                    RowBox[{"(*", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Echo", "@", 
                    RowBox[{"Select", "[", 
                    RowBox[{"#", ",", 
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    "#1", "\[LeftDoubleBracket]", "2", 
                    "\[RightDoubleBracket]"}], "!"}], "\[Tilde]", "Null"}], 
                    "&"}]}], "]"}]}], "&"}], "@"}], "*)"}], 
                    "\[IndentingNewLine]", 
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"Priority", "[", 
                    RowBox[{"parser", ",", "#"}], "]"}], ",", 
                    RowBox[{"RegexpToNonTerminal", "[", 
                    RowBox[{"parser", ",", "#1"}], "]"}]}], "}"}], "&"}], ",",
                     "#"}], "]"}]}], "&"}], "@", "\[IndentingNewLine]", 
                   RowBox[{"Select", "[", 
                    RowBox[{"#", ",", "StringQ"}], "]"}]}], "&"}], "@", 
                 "\[IndentingNewLine]", 
                 RowBox[{"Map", "[", 
                  RowBox[{"Metadata", ",", "#"}], "]"}]}], "&"}], "@", 
               "\[IndentingNewLine]", 
               RowBox[{"Sow", "[", 
                RowBox[{"Last", "[", 
                 RowBox[{
                  RowBox[{"Select", "[", 
                   RowBox[{"stack", ",", 
                    RowBox[{
                    RowBox[{"#", "~", "Intersects", "~", 
                    RowBox[{"AcceptStates", "[", "nfa", "]"}]}], "&"}]}], " ",
                    "]"}], ",", 
                  RowBox[{"Throw", "[", 
                   RowBox[{
                    RowBox[{"\"\<Ungrammatical string at position \>\"", "<>", 
                    RowBox[{"ToString", "@", "index"}], "<>", 
                    "\"\<. Character \>\"", "<>", 
                    RowBox[{
                    "source", "\[LeftDoubleBracket]", "index", 
                    "\[RightDoubleBracket]"}]}], ",", 
                    "UngrammaticalStringError"}], "]"}]}], "]"}], "]"}]}], 
              "\[IndentingNewLine]", "]"}]}], ";", "\[IndentingNewLine]", 
            "\[IndentingNewLine]", 
            RowBox[{"(*", " ", 
             RowBox[{
              RowBox[{"bugfix", ":", " ", 
               RowBox[{"Last", " ", "position"}]}], ",", " ", 
              RowBox[{"not", " ", "first", " ", 
               RowBox[{"(", 
                RowBox[{
                "last", " ", "position", " ", "means", " ", "longer", " ", 
                 "matched", " ", "string"}], ")"}]}]}], " ", "*)"}], 
            "\[IndentingNewLine]", 
            RowBox[{"index", " ", "=", " ", 
             RowBox[{"index", "-", 
              RowBox[{"(", 
               RowBox[{
                RowBox[{"Length", "[", "stack", "]"}], "-", 
                RowBox[{"Last", "@", 
                 RowBox[{"Flatten", "@", 
                  RowBox[{"Position", "[", 
                   RowBox[{"stack", ",", "lastAccept"}], "]"}]}]}]}], 
               ")"}]}]}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
            RowBox[{"tokens", "\[LeftArrow]", 
             RowBox[{"Token", "[", 
              RowBox[{"nonTerminal", ",", 
               RowBox[{"StringJoin", "[", 
                RowBox[{"source", "\[LeftDoubleBracket]", 
                 RowBox[{"lastMatchIndex", ";;", 
                  RowBox[{"index", "-", "1"}]}], "\[RightDoubleBracket]"}], 
                "]"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
            RowBox[{"lastMatchIndex", "=", "index"}], ";"}]}], 
          "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", "tokens"}]}], "\[IndentingNewLine]", "]"}]}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Tokenizer", "[", "nfa", "]"}]}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"e58d956c-770c-415a-81f7-84580ea26e36"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EndPackage", "[", "]"}], ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"0914e130-6b29-4b2a-9c08-7cfe7a7e0a39"],

Cell[CellGroupData[{

Cell["Test", "Section",ExpressionUUID->"35a6c047-a519-45f1-a23b-c82911d60ebc"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", 
  RowBox[{"NotebookDirectory", "[", "]"}], "]"}]], "Input",ExpressionUUID->\
"86272453-3260-4d2d-84d3-af7ab6834fd9"],

Cell[BoxData["\<\"/home/vitor/.Mathematica/Applications/LexerGenerator\"\>"], \
"Output",ExpressionUUID->"24a17b4c-ff56-4814-a32c-215194b5f839"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ReadString", "[", "\"\<test.txt\>\"", "]"}]], "Input",ExpressionUUID->"0efb9d92-c05d-4cef-8a30-2b8ff9b5784c"],

Cell[BoxData["\<\"nota -> \\\"do|re|mi|fa|sol|la|si\\\"\\n_ -> \\\" \
+\\\"\\nlinefeed -> \\\"\\\\n|\\\\r\\\\n\\\"\\nduracao -> \
\\\"/(2|4)\\\"\\ncomando -> \\\"mais +(rapido|lento)|(sobe|desce) +oitava\\\"\
\\ncoringa -> \\\".abc\\\"\\n\"\>"], "Output",ExpressionUUID->"c5849cd0-0539-\
4b8d-91a9-be8c06d1c9be"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"n", "=", 
  RowBox[{"BuildTokenizer", "[", 
   RowBox[{"ReadString", "[", "\"\<test.txt\>\"", "]"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"n", "[", "\"\<do re \n\nmi/4 dabc\>\"", "]"}], "//", 
  "MatrixForm"}]}], "Input",ExpressionUUID->"9b96a081-4d9a-4364-a4b4-\
626dcaec0c83"],

Cell[BoxData[
 RowBox[{"Tokenizer", "[", 
  RowBox[{
  "Automaton`NFABuilder`NFA", 
   "[", "\<\"60b8250a-9ac8-4466-8774-df8dbcc68a54\"\>", "]"}], 
  "]"}]], "Output",ExpressionUUID->"5c876eb3-4b54-49c2-abfc-12001db50b96"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", 
   TagBox[GridBox[{
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"do\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"re\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"nota\"\>", ",", "\<\"mi\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"duracao\"\>", ",", "\<\"/4\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"_\"\>", ",", "\<\" \"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"coringa\"\>", ",", "\<\"dabc\"\>"}], "]"}]}
     },
     GridBoxAlignment->{
      "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
        "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Column], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",ExpressionUUID->"bb04c944-4439-472c-\
b2d8-634d698c261d"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1366, 716},
WindowMargins->{{0, Automatic}, {0, Automatic}},
DockedCells->FEPrivate`FrontEndResource[
 "NotebookTemplatingExpressions", "AuthoringDockedCell"],
TaggingRules->{"NotebookTemplateVersion" -> 1., "NotebookTemplate" -> True},
CellContext->Notebook,
InputAliases->{"intt" -> RowBox[{"\[Integral]", 
     RowBox[{"\[SelectionPlaceholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "dintt" -> 
  RowBox[{
     SubsuperscriptBox[
     "\[Integral]", "\[SelectionPlaceholder]", "\[Placeholder]"], 
     RowBox[{"\[Placeholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "sumt" -> RowBox[{
     UnderoverscriptBox["\[Sum]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "prodt" -> RowBox[{
     UnderoverscriptBox["\[Product]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "dt" -> RowBox[{
     SubscriptBox["\[PartialD]", "\[Placeholder]"], " ", 
     "\[SelectionPlaceholder]"}], "ia" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Inactive"], "cbrt" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "3", SurdForm -> True, MultilineFunction -> 
    None], "surd" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "\[Placeholder]", SurdForm -> True, 
    MultilineFunction -> None], "ket" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Ket"], "bra" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Bra"], "braket" -> 
  TemplateBox[{"\[SelectionPlaceholder]", "\[Placeholder]"}, "BraKet"], 
  "delay" -> TemplateBox[{"\[SelectionPlaceholder]"}, "SystemsModelDelay"], 
  "grad" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Placeholder]"}], 
  "del." -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], ".", 
     "\[Placeholder]"}], "delx" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Cross]", 
     "\[Placeholder]"}], "del2" -> RowBox[{
     SubsuperscriptBox["\[Del]", "\[SelectionPlaceholder]", 2], 
     "\[Placeholder]"}], "kd" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "KroneckerDeltaSeq"], "notation" -> 
  RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation>" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation<" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "symb" -> RowBox[{"Symbolize", "[", 
     TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], "]"}], 
  "infixnotation" -> RowBox[{"InfixNotation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], ",", 
       "\[Placeholder]"}], "]"}], "addia" -> RowBox[{"AddInputAlias", "[", 
     RowBox[{"\"\[SelectionPlaceholder]\"", "\[Rule]", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "pattwraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationPatternTag"], 
  "madeboxeswraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationMadeBoxesTag"], "upa" -> 
  RowBox[{"\[Placeholder]", "\[LeftArrow]", "\[Placeholder]"}], "uupa" -> 
  RowBox[{"\[Placeholder]", 
     OverscriptBox["\[LeftArrow]", "\[Placeholder]"], "\[Placeholder]"}]},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (March 13, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 258, 6, 50, "Input", "ExpressionUUID" -> \
"a13b7e82-d940-4731-a11d-651909fe5a62",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[841, 30, 553, 13, 73, "Input", "ExpressionUUID" -> \
"85ec3a26-d5ba-4c14-9626-b3228652dd29",
 InitializationCell->True],
Cell[1397, 45, 628, 11, 44, "Message", "ExpressionUUID" -> \
"da5959f6-e785-4ff0-8133-abc09d2f3b57"]
}, Open  ]],
Cell[2040, 59, 843, 23, 188, "Input", "ExpressionUUID" -> \
"5969376e-9eef-493c-bef7-e7b8f4aa0f23",
 InitializationCell->True],
Cell[2886, 84, 177, 4, 50, "Input", "ExpressionUUID" -> \
"eff3517b-18cd-487a-ad96-a6ea2d12298e",
 InitializationCell->True],
Cell[3066, 90, 203, 6, 50, "Input", "ExpressionUUID" -> \
"57e33626-0b4a-49c8-90b2-15a492fa5a99",
 InitializationCell->True],
Cell[3272, 98, 1908, 47, 278, "Input", "ExpressionUUID" -> \
"b6d585a1-0b3d-4b97-8fb6-53d17c2f471e",
 InitializationCell->True],
Cell[5183, 147, 1040, 26, 140, "Input", "ExpressionUUID" -> \
"4a42fb47-283c-426b-9a4a-3f3ddfdd554c",
 InitializationCell->True],
Cell[6226, 175, 1069, 27, 141, "Input", "ExpressionUUID" -> \
"47ee3f8f-c530-4ee8-a186-63fd46ac3f82",
 InitializationCell->True],
Cell[7298, 204, 3937, 99, 482, "Input", "ExpressionUUID" -> \
"a424bca6-190f-4576-a3da-2d077f4fc0bb",
 InitializationCell->True],
Cell[11238, 305, 2710, 66, 457, "Input", "ExpressionUUID" -> \
"2346b4de-9915-4eac-b906-c8b14df68b55",
 InitializationCell->True],
Cell[13951, 373, 9749, 216, 936, "Input", "ExpressionUUID" -> \
"e58d956c-770c-415a-81f7-84580ea26e36",
 InitializationCell->True],
Cell[23703, 591, 233, 6, 73, "Input", "ExpressionUUID" -> \
"0914e130-6b29-4b2a-9c08-7cfe7a7e0a39",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[23961, 601, 78, 0, 65, "Section", "ExpressionUUID" -> \
"35a6c047-a519-45f1-a23b-c82911d60ebc"],
Cell[CellGroupData[{
Cell[24064, 605, 161, 3, 32, "Input", "ExpressionUUID" -> \
"86272453-3260-4d2d-84d3-af7ab6834fd9"],
Cell[24228, 610, 144, 1, 32, "Output", "ExpressionUUID" -> \
"24a17b4c-ff56-4814-a32c-215194b5f839"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24409, 616, 133, 1, 32, "Input", "ExpressionUUID" -> \
"0efb9d92-c05d-4cef-8a30-2b8ff9b5784c"],
Cell[24545, 619, 312, 4, 165, "Output", "ExpressionUUID" -> \
"c5849cd0-0539-4b8d-91a9-be8c06d1c9be"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24894, 628, 323, 8, 99, "Input", "ExpressionUUID" -> \
"9b96a081-4d9a-4364-a4b4-626dcaec0c83"],
Cell[25220, 638, 222, 5, 32, "Output", "ExpressionUUID" -> \
"5c876eb3-4b54-49c2-abfc-12001db50b96"],
Cell[25445, 645, 1754, 48, 254, "Output", "ExpressionUUID" -> \
"bb04c944-4439-472c-b2d8-634d698c261d"]
}, Open  ]]
}, Open  ]]
}
]
*)

