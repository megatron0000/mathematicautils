(* Mathematica Package *)
(* Created by Mathematica Plugin for IntelliJ IDEA *)

(* :Title: RegexpCollectionLexer *)
(* :Context: RegexpCollectionLexer` *)
(* :Author: vitor *)
(* :Date: 2018-05-26 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2018 vitor *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["LexerGenerator`RegexpCollectionLexer`"];
(* Exported symbols added here with SymbolName::usage *)

Tokenize::usage =
    "Tokenize[stringRegexpCollection] returns a list of tokens.

  (Private) Tokenize[lexer] runs a lexer instance, returning a list of tokens and exhausting the lexer";

Lexer::usage =
    "Lexer[stringRegexpCollection] returns a Lexer[stringRegexpCollection,id]";

Begin["`Private`"];

<< GlobalThings`;
<< CFGrammar`;

Lexer[string_?StringQ] := Block[
  {lexer = Lexer[string, CreateUUID[]]},
  Lexer /: CharList[lexer] = StringSplit[string, ""];
  Lexer /: Index[lexer] = 1;
  lexer
];

Lexer /: Tokenize[lexer_Lexer] :=
    First@Last@Reap[While[
      Not[ Lookahead[lexer, 1] ~ MatchQ ~ EOF[_] ],
      SkipWS[lexer];
      Which[
        Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\w",
        Sow[Token["word", ReadWord[lexer]]],
        Lookahead[lexer, 1] ~ MatchQ ~ "\"",
        Sow[Token["string", ReadCharString[lexer]]],
        Lookahead[lexer, 1] ~ MatchQ ~ "-",
        Sow[Token["arrow", ReadArrow[lexer]]],
        Lookahead[lexer, 1] ~ MatchQ ~ "|",
        Sow[Token["verticalSeparator", ReadVerticalSeparator[lexer]]],
        Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\n|\\r\\n",
        Sow[Token["linefeed", ReadLinefeed[lexer]]],
        True,
        Throw[
          StringForm["Unknown Token starting with `1`", Style[Lookahead[lexer, 1], Green]],
          UnrecognizedToken
        ]
      ];
    ]];

Lexer /: ReadWord[lexer_Lexer] := Block[
  {word = ""},
  If[
    Not[ Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\w" ],
    Throw[
      StringForm[
        "Expected word beginning. Got `1`. At position `2`",
        Style[Lookahead[lexer, 1], Green],
        Style[Index[lexer], Green]
      ],
      UnexpectedCharacterError
    ]
  ];
  While[
    Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\w",
    word = word <> Lookahead[lexer, 1];
    Consume[lexer];
  ];
  word
];

(* Internal method of ReadCharString *)

Lexer /: ReadCharString["ReadEscapeSequence"][lexer_Lexer] := Block[
  {knownEscapes = <|"\\\"" -> "\""|>,
    currentSequence =
        StringJoin[Lookahead[lexer, 1], Lookahead[lexer, 2]]},
  Consume[lexer];
  Consume[lexer];
  If[Not[ knownEscapes[currentSequence] ~ MatchQ ~ Missing[__] ],
    knownEscapes[currentSequence],
    currentSequence
  ]
];

Lexer /: ReadCharString[lexer_Lexer] := Block[
  {string = ""},
  If[
    Not[ Lookahead[lexer, 1] ~ MatchQ ~ "\"" ],
    Throw[
      StringForm[
        "Expected `1` to open string. Got `2`. At position `3`",
        Style["\"", Green],
        Style[Lookahead[lexer, 1], Green],
        Style[Index[lexer], Green]
      ],
      UnexpectedCharacterError
    ],
    Consume[lexer];
  ];
  While[
    Not[ Lookahead[lexer, 1] ~ MatchQ ~ "\"" ],
    If[
      Lookahead[lexer, 1] ~ MatchQ ~ EOF[_],
      Throw[
        StringForm[
          "Unclosed string. Partial: `1`. At position `2`",
          Style[string, Green],
          Style[Index[lexer], Green]
        ],
        UnclosedStringError
      ]
    ];
    If[
      Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\n|\\r\\n",
      Throw[
        StringForm[
          "String cannot contain linefeed. Partial: `1`. At position `2`",
          Style[string, Green],
          Style[Index[lexer], Green]
        ],
        UnexpectedCharacterError
      ]
    ];
    (* Jump over \\ signaling escaped \"*)
    If[Lookahead[lexer, 1] ~ MatchQ ~ "\\" \[And] Lookahead[lexer, 2] ~ MatchQ ~ "\"",
      Consume[lexer];
    ];
    If[Lookahead[lexer, 1] ~ MatchQ ~ "\\",
      string = string <> ReadCharString["ReadEscapeSequence"][lexer],
      string = string <> Lookahead[lexer, 1]; Consume[lexer];
    ]
  ];
  Consume[lexer];
  string
];

Lexer /: ReadArrow[lexer_Lexer] := If[
  Not[Lookahead[lexer, 1] ~ MatchQ ~ "-"] \[Or] Not[ Lookahead[lexer, 2] ~ MatchQ ~ ">" ],
  Throw[
    StringForm[
      "Expected arrow. Got `1`. At position `2`",
      Style[Lookahead[lexer, 1] <> Lookahead[lexer, 2], Green],
      Style[Index[lexer], Green]
    ],
    UnexpectedCharacterError
  ],
  Consume[lexer]; Consume[lexer];
  "->"
];

Lexer /: ReadVerticalSeparator[lexer_Lexer] := If[
  Not[ Lookahead[lexer, 1] ~ MatchQ ~ "|" ],
  Throw[
    StringForm[
      "Expected |. Got `1`. At position `2`",
      Style[Lookahead[lexer, 1], Green],
      Style[Index[lexer], Green]
    ],
    UnexpectedCharacterError
  ],
  Consume[lexer];
  "|"
];

Lexer /: ReadLinefeed[lexer_Lexer] := Block[
  {result = ""},
  If[
    Not[Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\n" \[Or]
      (Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\r" \[And] Lookahead[lexer, 2] ~ MatchRegexpQ ~ "\\n")
    ],
    Throw[
      StringForm[
        "Expected linefeed. Got `1`. At position `2`",
        Style[Lookahead[lexer, 1], Green],
        Style[Index[lexer], Green]
      ],
      UnexpectedCharacterError
    ]
  ];
  While[
    True,
    Which[
      Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\n",
      result = result <> "\n"; Consume[lexer],

      Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\r" \[And] Lookahead[lexer, 2] ~ MatchRegexpQ ~ "\\n",
      result = result <> "\r\n"; Consume[lexer],

      Lookahead[lexer, 1] ~ MatchRegexpQ ~ "\\r" \[And] Not[ Lookahead[lexer, 2] ~ MatchRegexpQ ~ "\\n" ],
      Throw[
        StringForm[
          "Malformed \\r\\n. At position `1`",
          Style[Index[lexer], Green]
        ],
        UnexpectedCharacterError
      ],

      True,
      Break[]
    ];
    SkipWS[lexer];
  ];
  result
];

Lexer /: SkipWS[lexer_Lexer] := While[
  Lookahead[lexer, 1] ~ MatchRegexpQ ~ "^(?!\\n)[\\s\\t]",
  Consume[lexer];
];

Lexer /: Lookahead[lexer_Lexer, n_] := If[
  Index[lexer] + n - 1 > Length@CharList[lexer],
  EOF[Index[lexer] + n - 1 - Length@CharList[lexer] ],
  CharList[lexer][[Index[lexer] + n - 1]]
];

Lexer /: Consume[lexer_Lexer] := If[
  Lookahead[lexer, 1] ~ MatchQ ~ EOF[_],
  Throw["Reached EOF. No token to consume", NoRemainingTokenError],
  Lexer /: Index[lexer] = Index[lexer] + 1;
];

Tokenize[string_?StringQ] := Tokenize[Lexer[string]];

End[]; (* `Private` *)

EndPackage[];
