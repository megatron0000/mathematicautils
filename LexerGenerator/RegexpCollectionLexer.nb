(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     37676,        991]
NotebookOptionsPosition[     30126,        819]
NotebookOutlinePosition[     34000,        902]
CellTagsIndexPosition[     33957,        899]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"BeginPackage", "[", 
   RowBox[{"\"\<LexerGenerator`RegexpCollectionLexer`\>\"", ",", 
    RowBox[{"{", "\"\<LoadPackage`\>\"", "}"}]}], "]"}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"673ffffb-a0d6-4571-bf34-2574ebd466e7"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<GlobalThings`\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"LoadPackage", "@", "\"\<CFGrammar`\>\""}], ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"91c931dc-31c0-4467-aafd-73b418e58dba"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Tokenize", "::", "usage"}], "=", 
    "\"\<Tokenize[stringRegexpCollection] returns a list of tokens.\n\n\
(Private) Tokenize[lexer] runs a lexer instance, returning a list of tokens \
and exhausting the lexer\>\""}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Lexer", "::", "usage"}], "=", 
   "\"\<Lexer[stringRegexpCollection] returns a \
Lexer[stringRegexpCollection,id]\>\""}], ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"dc3e1058-b9e6-4944-b0ac-8471501d3a94"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"d3208245-3eb9-4a3e-8ef2-1bbc4a7a51d1"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Lexer", "[", 
   RowBox[{"string_", "?", "StringQ"}], "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"lexer", "=", 
      RowBox[{"Lexer", "[", 
       RowBox[{"string", ",", " ", 
        RowBox[{"CreateUUID", "[", "]"}]}], "]"}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Lexer", "/:", 
      RowBox[{"CharList", "[", "lexer", "]"}], "=", 
      RowBox[{"StringSplit", "[", 
       RowBox[{"string", ",", "\"\<\>\""}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Lexer", "/:", 
      RowBox[{"Index", "[", "lexer", "]"}], "=", "1"}], ";", 
     "\[IndentingNewLine]", "lexer"}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"12a47f3d-1006-4f70-a89d-e03e28b81f02"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"Tokenize", "[", "lexer_Lexer", "]"}], ":=", "\[IndentingNewLine]", 
  
  RowBox[{"First", "@", 
   RowBox[{"Last", "@", 
    RowBox[{"Reap", "[", 
     RowBox[{"While", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Lookahead", "[", 
          RowBox[{"lexer", ",", "1"}], "]"}], "!"}], "\[Tilde]", 
        RowBox[{"EOF", "[", "_", "]"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"SkipWS", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"Which", "[", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], 
           OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\w\>\""}], ",", 
          RowBox[{"Sow", "[", 
           RowBox[{"Token", "[", 
            RowBox[{"\"\<word\>\"", ",", 
             RowBox[{"ReadWord", "[", "lexer", "]"}]}], "]"}], "]"}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", "\"\<\\\"\>\""}], 
          ",", 
          RowBox[{"Sow", "[", 
           RowBox[{"Token", "[", 
            RowBox[{"\"\<string\>\"", ",", 
             RowBox[{"ReadCharString", "[", "lexer", "]"}]}], "]"}], "]"}], 
          ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", "\"\<-\>\""}], 
          ",", 
          RowBox[{"Sow", "[", 
           RowBox[{"Token", "[", 
            RowBox[{"\"\<arrow\>\"", ",", 
             RowBox[{"ReadArrow", "[", "lexer", "]"}]}], "]"}], "]"}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", "\"\<|\>\""}], 
          ",", 
          RowBox[{"Sow", "[", 
           RowBox[{"Token", "[", 
            RowBox[{"\"\<verticalSeparator\>\"", ",", 
             RowBox[{"ReadVerticalSeparator", "[", "lexer", "]"}]}], "]"}], 
           "]"}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], 
           OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n|\\\\r\\\\n\>\""}], 
          ",", 
          RowBox[{"Sow", "[", 
           RowBox[{"Token", "[", 
            RowBox[{"\"\<linefeed\>\"", ",", 
             RowBox[{"ReadLinefeed", "[", "lexer", "]"}]}], "]"}], "]"}], ",",
           "\[IndentingNewLine]", "True", ",", 
          RowBox[{"Throw", "[", 
           RowBox[{"\"\<Unknown Token starting with \>\"", "<>", 
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "1"}], "]"}]}], "]"}]}], 
         "\[IndentingNewLine]", "]"}], ";"}]}], "\[IndentingNewLine]", "]"}], 
     "]"}]}]}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"59c3c1fb-0847-4b7d-8651-968dd9f73bd6"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"ReadWord", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"word", "=", "\"\<\>\""}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"If", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Lookahead", "[", 
          RowBox[{"lexer", ",", "1"}], "]"}], "!"}], 
        OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\w\>\""}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Throw", "[", 
        RowBox[{
         RowBox[{"\"\<Expected word beginning. Got\>\"", "<>", 
          RowBox[{"Lookahead", "[", 
           RowBox[{"lexer", ",", "1"}], "]"}], "<>", "\"\<.At position \>\"", 
          "<>", 
          RowBox[{"ToString", "@", 
           RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
         "UnexpectedCharacterError"}], "]"}]}], "\[IndentingNewLine]", "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"While", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"Lookahead", "[", 
         RowBox[{"lexer", ",", "1"}], "]"}], 
        OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\w\>\""}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"word", "=", 
         RowBox[{"word", "<>", 
          RowBox[{"Lookahead", "[", 
           RowBox[{"lexer", ",", "1"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"Consume", "[", "lexer", "]"}], ";"}]}], 
      "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", "word"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"e5bd0452-19d7-458c-a7fa-00031abda8fc"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Internal", " ", "method", " ", "of", " ", "ReadCharString"}], " ",
    "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Lexer", "/:", 
     RowBox[{
      RowBox[{"ReadCharString", "[", "\"\<ReadEscapeSequence\>\"", "]"}], "[",
       "lexer_Lexer", "]"}], ":=", 
     RowBox[{"Block", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"knownEscapes", "=", 
          RowBox[{"<|", 
           RowBox[{"\"\<\\\\\\\"\>\"", "\[Rule]", "\"\<\\\"\>\""}], "|>"}]}], 
         ",", 
         RowBox[{"currentSequence", "=", 
          RowBox[{"StringJoin", "[", 
           RowBox[{
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "1"}], "]"}], ",", 
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "2"}], "]"}]}], "]"}]}]}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Consume", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"Consume", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"knownEscapes", "[", "currentSequence", "]"}], "!"}], 
           "\[Tilde]", 
           RowBox[{"Missing", "[", "__", "]"}]}], ",", "\[IndentingNewLine]", 
          
          RowBox[{"knownEscapes", "[", "currentSequence", "]"}], ",", 
          "\[IndentingNewLine]", "currentSequence"}], "\[IndentingNewLine]", 
         "]"}]}]}], "\[IndentingNewLine]", "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"Lexer", "/:", 
    RowBox[{"ReadCharString", "[", "lexer_Lexer", "]"}], ":=", 
    RowBox[{"Block", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"string", "=", "\"\<\>\""}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"If", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], "!"}], 
          OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\"\>\""}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"Throw", "[", 
          RowBox[{
           RowBox[{"\"\<Expected \\\" to open string. Got \>\"", "<>", 
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "1"}], "]"}], "<>", 
            "\"\<.At position \>\"", "<>", 
            RowBox[{"ToString", "@", 
             RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
           "UnexpectedCharacterError"}], "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Consume", "[", "lexer", "]"}], ";"}]}], 
        "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
       RowBox[{"While", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], "!"}], "\[Tilde]", 
          "\"\<\\\"\>\""}], ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"If", "[", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", 
             RowBox[{"EOF", "[", "_", "]"}]}], ",", "\[IndentingNewLine]", 
            RowBox[{"Throw", "[", 
             RowBox[{
              RowBox[{
              "\"\<Unclosed string. Partial: \>\"", "<>", "string", "<>", 
               "\"\<.At position \>\"", "<>", 
               RowBox[{"ToString", "@", 
                RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
              "UnclosedStringError"}], "]"}]}], "\[IndentingNewLine]", "]"}], 
          ";", "\[IndentingNewLine]", 
          RowBox[{"If", "[", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "1"}], "]"}], 
             OverscriptBox["\[Tilde]", "\:30ec"], 
             "\"\<\\\\n|\\\\r\\\\n\>\""}], ",", "\[IndentingNewLine]", 
            RowBox[{"Throw", "[", 
             RowBox[{
              RowBox[{
              "\"\<String cannot contain linefeed. Partial: \>\"", "<>", 
               "string", "<>", "\"\<.At position \>\"", "<>", 
               RowBox[{"ToString", "@", 
                RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
              "UnexpectedCharacterError"}], "]"}]}], "\[IndentingNewLine]", 
           "]"}], ";", "\[IndentingNewLine]", 
          RowBox[{"(*", " ", 
           RowBox[{
           "Jump", " ", "over", " ", "\\\\", " ", "signaling", " ", "escaped",
             " ", "\\\""}], "*)"}], "\[IndentingNewLine]", 
          RowBox[{"If", "[", 
           RowBox[{
            RowBox[{
             RowBox[{
              RowBox[{"Lookahead", "[", 
               RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", 
              "\"\<\\\\\>\""}], "\[And]", 
             RowBox[{
              RowBox[{"Lookahead", "[", 
               RowBox[{"lexer", ",", "2"}], "]"}], "\[Tilde]", 
              "\"\<\\\"\>\""}]}], ",", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"Consume", "[", "lexer", "]"}], ";"}]}], 
           "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
          RowBox[{"If", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", 
             "\"\<\\\\\>\""}], ",", "\[IndentingNewLine]", 
            RowBox[{"string", "=", 
             RowBox[{"string", "<>", 
              RowBox[{
               RowBox[{
               "ReadCharString", "[", "\"\<ReadEscapeSequence\>\"", "]"}], 
               "[", "lexer", "]"}]}]}], ",", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"string", "=", 
              RowBox[{"string", "<>", 
               RowBox[{"Lookahead", "[", 
                RowBox[{"lexer", ",", "1"}], "]"}]}]}], ";", 
             RowBox[{"Consume", "[", "lexer", "]"}], ";"}]}], 
           "\[IndentingNewLine]", "]"}]}]}], "\[IndentingNewLine]", "]"}], 
       ";", "\[IndentingNewLine]", 
       RowBox[{"Consume", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
       "string"}]}], "\[IndentingNewLine]", "]"}]}]}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"1306e190-56de-49b4-bf09-f86a46719a1e"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"ReadArrow", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "1"}], "]"}], "!"}], "\[Tilde]", "\"\<-\>\""}], 
     "\[Or]", 
     RowBox[{
      RowBox[{
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "2"}], "]"}], "!"}], "\[Tilde]", 
      "\"\<>\>\""}]}], ",", "\[IndentingNewLine]", 
    RowBox[{"Throw", "[", 
     RowBox[{
      RowBox[{"\"\<Expected arrow. Got \>\"", "<>", 
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "1"}], "]"}], "<>", 
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "2"}], "]"}], "<>", "\"\<. At position \>\"", "<>", 
       RowBox[{"ToString", "@", 
        RowBox[{"Index", "[", "lexer", "]"}]}]}], " ", ",", 
      "UnexpectedCharacterError"}], "]"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Consume", "[", "lexer", "]"}], ";", 
     RowBox[{"Consume", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
     "\"\<->\>\""}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"67e81cab-50f3-40fc-bb99-2d6aa1fbb344"],

Cell[BoxData[
 RowBox[{"Lexer", " ", "/:", 
  RowBox[{"ReadVerticalSeparator", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"Lookahead", "[", 
       RowBox[{"lexer", ",", "1"}], "]"}], "!"}], "\[Tilde]", "\"\<|\>\""}], 
    ",", "\[IndentingNewLine]", 
    RowBox[{"Throw", "[", 
     RowBox[{
      RowBox[{"\"\<Expected |. Got \>\"", "<>", 
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "1"}], "]"}], "<>", "\"\<. At position \>\"", "<>", 
       RowBox[{"ToString", "@", 
        RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
      "UnexpectedCharacterError"}], "]"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Consume", "[", "lexer", "]"}], ";", "\[IndentingNewLine]", 
     "\"\<|\>\""}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"eb2d2550-5f16-455c-8d5f-444dc5332ca8"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"ReadLinefeed", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"result", "=", "\"\<\>\""}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"If", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"!", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], 
           OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n\>\""}], "\[Or]", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "1"}], "]"}], 
             OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\r\>\""}], "\[And]", 
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "2"}], "]"}], 
             OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n\>\""}]}], 
           ")"}]}], ")"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"Throw", "[", 
        RowBox[{"\"\<Expected linefeed. Got \>\"", "<>", 
         RowBox[{"Lookahead", "[", 
          RowBox[{"lexer", ",", "1"}], "]"}]}], "]"}]}], 
      "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"While", "[", "\[IndentingNewLine]", 
      RowBox[{"True", ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Which", "[", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "1"}], "]"}], 
           OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n\>\""}], ",", 
          RowBox[{
           RowBox[{"result", "=", 
            RowBox[{"result", "<>", "\"\<\\n\>\""}]}], ";", 
           RowBox[{"Consume", "[", "lexer", "]"}]}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "1"}], "]"}], 
            OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\r\>\""}], "\[And]", 
           
           RowBox[{
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "2"}], "]"}], 
            OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n\>\""}]}], ",", 
          RowBox[{
           RowBox[{"result", "=", 
            RowBox[{"result", "<>", "\"\<\\r\\n\>\""}]}], ";", 
           RowBox[{"Consume", "[", "lexer", "]"}]}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{"Lookahead", "[", 
             RowBox[{"lexer", ",", "1"}], "]"}], 
            OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\r\>\""}], "\[And]", 
           
           RowBox[{
            RowBox[{
             RowBox[{"Lookahead", "[", 
              RowBox[{"lexer", ",", "2"}], "]"}], "!"}], 
            OverscriptBox["\[Tilde]", "\:30ec"], "\"\<\\\\n\>\""}]}], ",", 
          RowBox[{"Throw", "[", 
           RowBox[{
            RowBox[{
            "\"\<Malformed \\\\r\\\\n\>\"", "<>", "\"\<. At position \>\"", "<>", 
             RowBox[{"ToString", "@", 
              RowBox[{"Index", "[", "lexer", "]"}]}]}], ",", 
            "UnexpectedCharacterError"}], "]"}], ",", "\[IndentingNewLine]", 
          "True", ",", 
          RowBox[{"Break", "[", "]"}]}], "\[IndentingNewLine]", "]"}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"SkipWS", "[", "lexer", "]"}], ";"}]}], "\[IndentingNewLine]",
       "]"}], ";", "\[IndentingNewLine]", "result"}]}], "\[IndentingNewLine]",
    "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"58f1d76d-ffd7-4711-9b21-674a3288b09b"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"SkipWS", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"While", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Lookahead", "[", 
      RowBox[{"lexer", ",", "1"}], "]"}], 
     OverscriptBox["\[Tilde]", "\:30ec"], "\"\<^(?!\\\\n)[\\\\s\\\\t]\>\""}], 
    ",", 
    RowBox[{
     RowBox[{"Consume", "[", "lexer", "]"}], ";"}]}], "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"c37156e1-36bb-4060-93e8-3faadde08432"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"Lookahead", "[", 
   RowBox[{"lexer_Lexer", ",", "n_"}], "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"Index", "[", "lexer", "]"}], "+", "n", "-", "1"}], ">", 
     RowBox[{"Length", "@", 
      RowBox[{"CharList", "[", "lexer", "]"}]}]}], ",", "\[IndentingNewLine]", 
    RowBox[{"EOF", "[", 
     RowBox[{
      RowBox[{"Index", "[", "lexer", "]"}], "+", "n", "-", "1", "-", 
      RowBox[{"Length", "@", 
       RowBox[{"CharList", "[", "lexer", "]"}]}]}], " ", "]"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"CharList", "[", "lexer", "]"}], "\[LeftDoubleBracket]", 
     RowBox[{
      RowBox[{"Index", "[", "lexer", "]"}], "+", "n", "-", "1"}], 
     "\[RightDoubleBracket]"}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"833d65ec-17ff-4fdc-a2a0-53999a896b16"],

Cell[BoxData[
 RowBox[{"Lexer", "/:", 
  RowBox[{"Consume", "[", "lexer_Lexer", "]"}], ":=", 
  RowBox[{"If", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Lookahead", "[", 
      RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", 
     RowBox[{"EOF", "[", "_", "]"}]}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Throw", "[", 
      RowBox[{
      "\"\<Reached EOF. No token to consume\>\"", ",", 
       "NoRemainingTokenError"}], "]"}], ";"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Lexer", "/:", 
      RowBox[{"Index", "[", "lexer", "]"}], "=", 
      RowBox[{
       RowBox[{"Index", "[", "lexer", "]"}], "+", "1"}]}], ";"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"8acfe813-de88-49bc-b589-57a1bfad6e9e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Tokenize", "[", 
   RowBox[{"string_", "?", "StringQ"}], "]"}], ":=", 
  RowBox[{"Tokenize", "[", 
   RowBox[{"Lexer", "[", "string", "]"}], "]"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"f5b9ae11-1af8-4224-95d8-aeb914873230"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EndPackage", "[", "]"}], ";"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"759de2f4-1390-470b-9d4a-77de8591b6d8"],

Cell[CellGroupData[{

Cell["Test", "Section",ExpressionUUID->"f8676bd7-85ae-4151-8d60-2b09246a8d48"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"lex", "=", 
  RowBox[{
  "Lexer", "[", 
   "\"\<\\\"abc\\\\\\\"quote\\\\\\\" a\\\" b -> \\\"hello world\\\" as \n d \
\>\"", "]"}]}]], "Input",ExpressionUUID->"4f606599-4028-4908-b4d6-\
88327c680187"],

Cell[BoxData[
 RowBox[{"Lexer", "[", 
  RowBox[{"\<\"\\\"abc\\\\\\\"quote\\\\\\\" a\\\" b -> \\\"hello world\\\" as \
\\n d \"\>", ",", "\<\"ddc30e58-8267-4574-afca-d6439812fbeb\"\>"}], 
  "]"}]], "Output",ExpressionUUID->"17bb9dea-8cfe-413e-a505-50049325c213"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}]], "Input",ExpressionU\
UID->"0f248bdc-353b-44ae-b6ca-1ac6ea8bf44f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Tokenize", "[", 
   RowBox[{"Lexer", "[", 
    RowBox[{"ReadString", "[", "\"\<test.txt\>\"", "]"}], "]"}], "]"}], "//", 
  "MatrixForm"}]], "Input",ExpressionUUID->"8be617ff-912f-4dde-b877-\
1ca3baa94804"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", 
   TagBox[GridBox[{
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"nota\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", ",", "\<\"do|re|mi|fa|sol|la|si\"\>"}], 
        "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"_\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", ",", "\<\" +\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"linefeed\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", ",", "\<\"\\\\n|\\\\r\\\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"duracao\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", ",", "\<\"/(2|4)\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"comando\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", 
         ",", "\<\"mais +(rapido|lento)|(sobe|desce) +oitava\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"word\"\>", ",", "\<\"coringa\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"arrow\"\>", ",", "\<\"->\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"string\"\>", ",", "\<\".abc\"\>"}], "]"}]},
      {
       RowBox[{"CFGrammar`Token", "[", 
        RowBox[{"\<\"linefeed\"\>", ",", "\<\"\\n\"\>"}], "]"}]}
     },
     GridBoxAlignment->{
      "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
        "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Column], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",ExpressionUUID->"b344820c-53f1-4117-\
9fb8-f1514c95362d"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"While", "[", 
  RowBox[{
   RowBox[{"TrueQ", "[", 
    RowBox[{"EOF", "\[NotEqual]", " ", "\"\<a\>\""}], "]"}], ",", 
   RowBox[{"Echo", "[", "\"\<oi\>\"", "]"}]}], "]"}]], "Input",ExpressionUUID-\
>"c00fe845-ddd6-48f4-8faf-94587ebfb746"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Trace", "[", 
  RowBox[{"TrueQ", "[", 
   RowBox[{"EOF", "\[Equal]", "\"\<a\>\""}], "]"}], "]"}]], "Input",Expression\
UUID->"cffcbdd1-e62d-426b-9319-f0ea07a0131e"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   TagBox[
    RowBox[{"TrueQ", "[", 
     RowBox[{"EOF", "\[Equal]", "\<\"a\"\>"}], "]"}],
    HoldForm], ",", 
   TagBox["False",
    HoldForm]}], "}"}]], "Output",ExpressionUUID->"3476f47b-bebe-40c5-bdaa-\
1a3d079e3aa5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"HoldForm", "[", 
   RowBox[{
    RowBox[{"!", 
     RowBox[{
      RowBox[{"Lookahead", "[", 
       RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", "\"\<\\\"\>\""}]}], 
    "\[Or]", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"Lookahead", "[", 
        RowBox[{"lexer", ",", "1"}], "]"}], "\[Tilde]", "\"\<\\\"\>\""}], 
      "\[And]", 
      RowBox[{"!", 
       RowBox[{
        RowBox[{"Lookahead", "[", 
         RowBox[{"lexer", ",", "0"}], "]"}], "\[Tilde]", "\"\<\\\\\>\""}]}]}],
      ")"}]}], "]"}], "//", "FullForm"}]], "Input",ExpressionUUID->"51c9effc-\
6472-4ea6-adb1-3017e577ecc9"],

Cell[BoxData[
 TagBox[
  StyleBox[
   RowBox[{"HoldForm", "[", 
    RowBox[{"Or", "[", 
     RowBox[{
      RowBox[{"Not", "[", 
       RowBox[{"Tilde", "[", 
        RowBox[{
         RowBox[{"Lookahead", "[", 
          RowBox[{"lexer", ",", "1"}], "]"}], ",", "\"\<\\\"\>\""}], "]"}], 
       "]"}], ",", 
      RowBox[{"And", "[", 
       RowBox[{
        RowBox[{"Tilde", "[", 
         RowBox[{
          RowBox[{"Lookahead", "[", 
           RowBox[{"lexer", ",", "1"}], "]"}], ",", "\"\<\\\"\>\""}], "]"}], 
        ",", 
        RowBox[{"Not", "[", 
         RowBox[{"Tilde", "[", 
          RowBox[{
           RowBox[{"Lookahead", "[", 
            RowBox[{"lexer", ",", "0"}], "]"}], ",", "\"\<\\\\\>\""}], "]"}], 
         "]"}]}], "]"}]}], "]"}], "]"}],
   ShowSpecialCharacters->False,
   ShowStringCharacters->True,
   NumberMarks->True],
  FullForm]], "Output",ExpressionUUID->"61b621e8-50e7-49ca-bac1-790783c0c5ae"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\"\<\\n\>\"", 
  OverscriptBox["\[Tilde]", "\:30ec"], 
  "\"\<^(?!\\\\n)[\\\\s\\\\t]\>\""}]], "Input",ExpressionUUID->"2ed5b3a5-83d2-\
41ad-bf59-eb313c61d3fe"],

Cell[BoxData["False"], "Output",ExpressionUUID->"505951a7-fd1e-46f0-9104-d8a4cd17f70a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\"\<\\n\>\"", "//", 
  RowBox[{
   RowBox[{"StringSplit", "[", 
    RowBox[{"#", ",", "\"\<\>\""}], "]"}], "&"}]}]], "Input",ExpressionUUID->\
"0ef99647-c3a4-4b3e-b187-367f53dd28c5"],

Cell[BoxData[
 RowBox[{"{", "\<\"\\n\"\>", "}"}]], "Output",ExpressionUUID->"6e262bfd-2752-40cd-b802-cb600b6b1287"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{962, 533},
WindowMargins->{{162, Automatic}, {86, Automatic}},
InputAliases->{"intt" -> RowBox[{"\[Integral]", 
     RowBox[{"\[SelectionPlaceholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "dintt" -> 
  RowBox[{
     SubsuperscriptBox[
     "\[Integral]", "\[SelectionPlaceholder]", "\[Placeholder]"], 
     RowBox[{"\[Placeholder]", 
       RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}], "sumt" -> RowBox[{
     UnderoverscriptBox["\[Sum]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "prodt" -> RowBox[{
     UnderoverscriptBox["\[Product]", 
      RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], 
      "\[Placeholder]"], "\[Placeholder]"}], "dt" -> RowBox[{
     SubscriptBox["\[PartialD]", "\[Placeholder]"], " ", 
     "\[SelectionPlaceholder]"}], "ia" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Inactive"], "cbrt" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "3", SurdForm -> True, MultilineFunction -> 
    None], "surd" -> 
  RadicalBox[
   "\[SelectionPlaceholder]", "\[Placeholder]", SurdForm -> True, 
    MultilineFunction -> None], "ket" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Ket"], "bra" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "Bra"], "braket" -> 
  TemplateBox[{"\[SelectionPlaceholder]", "\[Placeholder]"}, "BraKet"], 
  "delay" -> TemplateBox[{"\[SelectionPlaceholder]"}, "SystemsModelDelay"], 
  "grad" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Placeholder]"}], 
  "del." -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], ".", 
     "\[Placeholder]"}], "delx" -> RowBox[{
     SubscriptBox["\[Del]", "\[SelectionPlaceholder]"], "\[Cross]", 
     "\[Placeholder]"}], "del2" -> RowBox[{
     SubsuperscriptBox["\[Del]", "\[SelectionPlaceholder]", 2], 
     "\[Placeholder]"}], "kd" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "KroneckerDeltaSeq"], "notation" -> 
  RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation>" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongRightArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "notation<" -> RowBox[{"Notation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], " ", 
       "\[DoubleLongLeftArrow]", " ", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "symb" -> RowBox[{"Symbolize", "[", 
     TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], "]"}], 
  "infixnotation" -> RowBox[{"InfixNotation", "[", 
     RowBox[{
       TemplateBox[{"\[SelectionPlaceholder]"}, "NotationTemplateTag"], ",", 
       "\[Placeholder]"}], "]"}], "addia" -> RowBox[{"AddInputAlias", "[", 
     RowBox[{"\"\[SelectionPlaceholder]\"", "\[Rule]", 
       TemplateBox[{"\[Placeholder]"}, "NotationTemplateTag"]}], "]"}], 
  "pattwraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationPatternTag"], 
  "madeboxeswraper" -> 
  TemplateBox[{"\[SelectionPlaceholder]"}, "NotationMadeBoxesTag"], "upa" -> 
  RowBox[{"\[Placeholder]", "\[LeftArrow]", "\[Placeholder]"}], "uupa" -> 
  RowBox[{"\[Placeholder]", 
     OverscriptBox["\[LeftArrow]", "\[Placeholder]"], "\[Placeholder]"}]},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (March 13, 2017)",
StyleDefinitions->FrontEnd`FileName[{"Report"}, "StandardReport.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 280, 6, 43, "Input", "ExpressionUUID" -> \
"673ffffb-a0d6-4571-bf34-2574ebd466e7",
 InitializationCell->True],
Cell[841, 28, 282, 7, 66, "Input", "ExpressionUUID" -> \
"91c931dc-31c0-4467-aafd-73b418e58dba",
 InitializationCell->True],
Cell[1126, 37, 595, 15, 155, "Input", "ExpressionUUID" -> \
"dc3e1058-b9e6-4944-b0ac-8471501d3a94",
 InitializationCell->True],
Cell[1724, 54, 177, 4, 43, "Input", "ExpressionUUID" -> \
"d3208245-3eb9-4a3e-8ef2-1bbc4a7a51d1",
 InitializationCell->True],
Cell[1904, 60, 848, 23, 156, "Input", "ExpressionUUID" -> \
"12a47f3d-1006-4f70-a89d-e03e28b81f02",
 InitializationCell->True],
Cell[2755, 85, 2996, 71, 357, "Input", "ExpressionUUID" -> \
"59c3c1fb-0847-4b7d-8651-968dd9f73bd6",
 InitializationCell->True],
Cell[5754, 158, 1724, 42, 395, "Input", "ExpressionUUID" -> \
"e5bd0452-19d7-458c-a7fa-00031abda8fc",
 InitializationCell->True],
Cell[7481, 202, 6417, 149, 1092, "Input", "ExpressionUUID" -> \
"1306e190-56de-49b4-bf09-f86a46719a1e",
 InitializationCell->True],
Cell[13901, 353, 1251, 31, 179, "Input", "ExpressionUUID" -> \
"67e81cab-50f3-40fc-bb99-2d6aa1fbb344",
 InitializationCell->True],
Cell[15155, 386, 944, 22, 179, "Input", "ExpressionUUID" -> \
"eb2d2550-5f16-455c-8d5f-444dc5332ca8",
 InitializationCell->True],
Cell[16102, 410, 3660, 87, 568, "Input", "ExpressionUUID" -> \
"58f1d76d-ffd7-4711-9b21-674a3288b09b",
 InitializationCell->True],
Cell[19765, 499, 477, 13, 53, "Input", "ExpressionUUID" -> \
"c37156e1-36bb-4060-93e8-3faadde08432",
 InitializationCell->True],
Cell[20245, 514, 948, 23, 133, "Input", "ExpressionUUID" -> \
"833d65ec-17ff-4fdc-a2a0-53999a896b16",
 InitializationCell->True],
Cell[21196, 539, 813, 21, 134, "Input", "ExpressionUUID" -> \
"8acfe813-de88-49bc-b589-57a1bfad6e9e",
 InitializationCell->True],
Cell[22012, 562, 279, 7, 43, "Input", "ExpressionUUID" -> \
"f5b9ae11-1af8-4224-95d8-aeb914873230",
 InitializationCell->True],
Cell[22294, 571, 233, 6, 66, "Input", "ExpressionUUID" -> \
"759de2f4-1390-470b-9d4a-77de8591b6d8",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[22552, 581, 78, 0, 67, "Section", "ExpressionUUID" -> \
"f8676bd7-85ae-4151-8d60-2b09246a8d48"],
Cell[CellGroupData[{
Cell[22655, 585, 224, 6, 64, "Input", "ExpressionUUID" -> \
"4f606599-4028-4908-b4d6-88327c680187"],
Cell[22882, 593, 261, 4, 65, "Output", "ExpressionUUID" -> \
"17bb9dea-8cfe-413e-a505-50049325c213"]
}, Open  ]],
Cell[23158, 600, 180, 4, 43, "Input", "ExpressionUUID" -> \
"0f248bdc-353b-44ae-b6ca-1ac6ea8bf44f"],
Cell[CellGroupData[{
Cell[23363, 608, 241, 6, 41, "Input", "ExpressionUUID" -> \
"8be617ff-912f-4dde-b877-1ca3baa94804"],
Cell[23607, 616, 3441, 92, 633, "Output", "ExpressionUUID" -> \
"b344820c-53f1-4117-9fb8-f1514c95362d"]
}, Open  ]],
Cell[27063, 711, 262, 6, 43, "Input", "ExpressionUUID" -> \
"c00fe845-ddd6-48f4-8faf-94587ebfb746"],
Cell[CellGroupData[{
Cell[27350, 721, 188, 4, 41, "Input", "ExpressionUUID" -> \
"cffcbdd1-e62d-426b-9319-f0ea07a0131e"],
Cell[27541, 727, 263, 9, 43, "Output", "ExpressionUUID" -> \
"3476f47b-bebe-40c5-bdaa-1a3d079e3aa5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[27841, 741, 650, 20, 43, "Input", "ExpressionUUID" -> \
"51c9effc-6472-4ea6-adb1-3017e577ecc9"],
Cell[28494, 763, 933, 28, 43, "Output", "ExpressionUUID" -> \
"61b621e8-50e7-49ca-bac1-790783c0c5ae"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29464, 796, 183, 4, 56, "Input", "ExpressionUUID" -> \
"2ed5b3a5-83d2-41ad-bf59-eb313c61d3fe"],
Cell[29650, 802, 87, 0, 52, "Output", "ExpressionUUID" -> \
"505951a7-fd1e-46f0-9104-d8a4cd17f70a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29774, 807, 206, 5, 52, "Input", "ExpressionUUID" -> \
"0ef99647-c3a4-4b3e-b187-367f53dd28c5"],
Cell[29983, 814, 115, 1, 81, "Output", "ExpressionUUID" -> \
"6e262bfd-2752-40cd-b802-cb600b6b1287"]
}, Open  ]]
}, Open  ]]
}
]
*)

